﻿angular.module("umbraco")
    .controller("WTC.LivePromoPickerController",
    function ($scope, assetsService, notificationsService, $compile, $http) {
        //this function will execute when all dependencies have loaded
        //notificationsService.success("Success", "Speaker has been deleted");


        $scope.savepromotion = function (pkgNumber) {
            $scope.model.value = pkgNumber;
        };

        $scope.mySplit = function (string, nb) {
            var array = string.split('|');
            return array[nb];
        }

        $scope.checkpromotion = function (ele) {
            if (!ele.target.parentElement.parentElement.children.promodasts.children.countriesddl.value) {
                notificationsService.error("Error", "you must pick destination");
                //alert('you must pick destination');
                return;
            }
            if (ele.target.parentElement.parentElement.children.promodates.children.promotodate.children.todate.value < ele.target.parentElement.parentElement.children.promodates.children.promofromdate.children.fromdate.value) {
                notificationsService.error("Error", "to date must be bigger then from date");
                //alert('to date must be bigger then from date');
                return;
            }

            $http.get("/umbraco/api/Main/GetSiteInfo")
.then(function (response) {
    if (response != null && response.data != null) {
        var apiaddress = response.data.appUrl + "site/promotions/"+ ele.target.parentElement.parentElement.children.promodates.children.promofromdate.children.fromdate.value + "/" + ele.target.parentElement.parentElement.children.promodates.children.promotodate.children.todate.value + "?destinationGW=" + ele.target.parentElement.parentElement.children.promodasts.children.countriesddl.value;
        apiaddress += "&APIKey=" + response.data.appAPIKey + "&lang = he-IL";
        console.info(apiaddress);
            //var apiaddress = "http://dev.app.algomasystems.com:8080/api/site/promotions/" + ele.target.parentElement.parentElement.children.promodates.children.promofromdate.children.fromdate.value + "/" + ele.target.parentElement.parentElement.children.promodates.children.promotodate.children.todate.value + "?destinationGW=" + ele.target.parentElement.parentElement.children.promodasts.children.countriesddl.value;
            //apiaddress += "&APIKey=111222333";
            $http.get(apiaddress)
            .then(function (response) {
                if (response != null && response.data != null && response.data.results != null) {
					response.data = response.data.results;
                    if (response.data.length > 0) {
                        var unLoadPackageMessage = "Promotion Count = " + response.data.length + " | ";
                        var anew = "<div class='mygrid-wrapper-div'><div class='col-md-12 mt-15'>";
                        $(response.data).each(function () {
                            if (!this.destinationData) {
                                return true;
                            }
                            try {
                                if (!this.media || !this.media.images) {
                                    unLoadPackageMessage += this.title + " no hotel image! | ";
                                }
                                var currentPromotion = "";
                                switch (this.segment) {
                                    case "PACKAGES":
                                    case "HOTELS":
                                     currentPromotion = "<div style='width:45%;float:left;margin-right:10px;'>\
                                    <div class='card " + ($scope.checkIfSelected(this.packageNumber) ? 'choosen-promo' : '') + "'><img class='card-img-top' src='" + this.media.images[0].src + "' alt='Card image cap'>\
                                    <div class='card-block row'><h4 class='card-title text-center' style='color:#2e1414'>" + this.title + "</h4>\
                                    <div style='width:50%;float:left;'>\
                                    <p style='text-align:center'>" + this.destinationData.name + "</p>\
                                    <p style='text-align:center'>roomType: " + this.roomType.name + "</p>\
                                    <p style='text-align:center'>pkg number: " + this.packageNumber + "</p>\
                                    <p style='text-align:center'>pkg code: " + this.code.split('|')[1] + "</p>\
                                    <p style='text-align:center'>" + this.fromDate + "</p>\
                                    </div><div style='width:50%;float:left;'>\
                                    <p style='text-align:center' title='" + this.mainProductName + "'>" + $scope.cutLongString(this.mainProductName) + "</p>\
                                    <p style='text-align:center'>nights: " + this.nights + "</p>\
                                    <p style='text-align:center'>adults: " + this.composition.adults + " | children: " + this.composition.children + "</p>\
                                    <p style='text-align:center'>" + this.boardBase.code + " - " + this.boardBase.name + "</p>\
                                    <p style='text-align:center'>" + this.toDate + "</p>\
                                    </div>\
                                    <h4 class='card-title text-center' style='color:#2e1414'>" + this.price.currency.symbol + this.price.total + "</h4>\
                                    <div style='display:inline-block;width:100%'>\
                                    <button ng-click='savepromotion(\"" + this.packageNumber + "|" + this.title + "\")' class='btn btn-primary btn-block' " + ($scope.checkIfSelected(this.packageNumber) ? 'disabled' : '') + ">choose promotion</button></div></div></div></div>";
                                        break;
                                    case "FLYANDDRIVE":
                                        currentPromotion = "<div style='width:45%;float:left;margin-right:10px;'>\
                                    <div class='card " + ($scope.checkIfSelected(this.packageNumber) ? 'choosen-promo' : '') + "'><img class='card-img-top' src='" + this.media.images[0].src + "' alt='Card image cap'>\
                                    <div class='card-block row'><h4 class='card-title text-center' style='color:#2e1414'>" + this.title + "</h4>\
                                    <div style='width:50%;float:left;'>\
                                    <p style='text-align:center'>" + this.destinationData.name + "</p>\
                                    <p style='text-align:center'>pkg number: " + this.packageNumber + "</p>\
                                    <p style='text-align:center'>Airline: " + this.outBoundFlightLegs[0].flights[0].airline.name + "</p>\
                                    <p style='text-align:center'>" + this.fromDate + "</p>\
                                    </div><div style='width:50%;float:left;'>\
                                    <p style='text-align:center' title='" + this.mainProductName + "'>" + $scope.cutLongString(this.mainProductName) + "</p>\
                                    <p style='text-align:center'>adults: " + this.composition.adults + " | children: " + this.composition.children + "</p>\
                                    <p style='text-align:center'>" + this.toDate + "</p>\
                                    </div>\
                                    <h4 class='card-title text-center' style='color:#2e1414'>" + this.price.currency.symbol + this.price.total + "</h4>\
                                    <div style='display:inline-block;width:100%'>\
                                    <button ng-click='savepromotion(\"" + this.packageNumber + "|" + this.title + "\")' class='btn btn-primary btn-block' " + ($scope.checkIfSelected(this.packageNumber) ? 'disabled' : '') + ">choose promotion</button></div></div></div></div>";
                                        break;
                                    case "FLIGHTS":
                                        currentPromotion = "<div style='width:45%;float:left;margin-right:10px;'>\
                                    <div class='card " + ($scope.checkIfSelected(this.packageNumber) ? 'choosen-promo' : '') + "'><img class='card-img-top' src='" + this.media.images[0].src + "' alt='Card image cap'>\
                                    <div class='card-block row'><h4 class='card-title text-center' style='color:#2e1414'>" + this.title + "</h4>\
                                    <div style='width:50%;float:left;'>\
                                    <p style='text-align:center'>" + this.destinationData.name + "</p>\
                                    <p style='text-align:center'>pkg number: " + this.packageNumber + "</p>\
                                    <p style='text-align:center'>Airline: " + this.outBoundFlightLegs[0].flights[0].airline.name + "</p>\
                                    <p style='text-align:center'>" + this.fromDate + "</p>\
                                    </div><div style='width:50%;float:left;'>\
                                    <p style='text-align:center'>adults: " + this.composition.adults + " | children: " + this.composition.children + "</p>\
                                    <p style='text-align:center'>" + this.toDate + "</p>\
                                    </div>\
                                    <h4 class='card-title text-center' style='color:#2e1414'>" + this.price.currency.symbol + this.price.total + "</h4>\
                                    <div style='display:inline-block;width:100%'>\
                                    <button ng-click='savepromotion(\"" + this.packageNumber + "|" + this.title + "\")' class='btn btn-primary btn-block' " + ($scope.checkIfSelected(this.packageNumber) ? 'disabled' : '') + ">choose promotion</button></div></div></div></div>";
                                        break;
                                    default:
                                }

                                debugger;

                                anew += currentPromotion;
                            } catch (e) {
                                console.error(e.message);
                            }
                        });
                        anew += "</div></div>";
                        $scope.html = anew;
                        notificationsService.error("Info", unLoadPackageMessage);
                    }
                    else {
                        notificationsService.info("Info", "no promotions found for this search");
                    }
                }
            });

    }
});

        };

        $scope.cutLongString = function (str) {
            if (str.length > 15) {
                return str.substring(0, 14) + "...";
            }
            return str;
        };

        $scope.checkIfSelected = function (pkgNumber) {
            if (pkgNumber == $scope.model.value.split('|')[0]) {
                return true;//"choosen-promo";
            }
            return false;
        };

        $http.get("/umbraco/api/Main/GetSiteInfo")
.then(function (response) {
    if (response != null && response.data != null) {
        var url = response.data.appUrl + "packages/destinations?APIKey=" + response.data.appAPIKey + "&lang = he-IL";
        console.info(url);
        $http.get(url)
            .then(function (response) {
                if (response != null && response.data != null && response.data.results != null) {
					response.data = response.data.results;

                    $('.countriesddl').find('option').remove();
                    if ($(".countriesddl > option").length == 0) {
                        $(".countriesddl")
                            .append($("<option></option>")
                            .attr("value", "")
                            .text("Choose"));

                        $(response.data).each(function () {
                            $(".countriesddl")
                                .append($("<option></option>")
                                .attr("value", $(this)[0].code)
                                .text($(this)[0].name));
                        });
                    }
                }
            });
    }
});

        //$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        $(".promodatepicker").attr("min", today);
        $(".promodatepicker").attr("value", today);
        //if (!$(".promodatepicker").attr("value")) {

        //}

        //load the separate css for the editor to avoid it blocking our js loading
        //assetsService.loadCss("/App_Plugins/LivePromoPicker/lib/bootstrap.css");

    }).directive('dynamic', function ($compile) {
        return {
            restrict: 'A',
            replace: true,
            link: function (scope, ele, attrs) {
                scope.$watch(attrs.dynamic, function (html) {
                    ele.html(html);
                    $compile(ele.contents())(scope);
                });
            }
        };
    });



function fromDateChanged(ele) {
    
    var susu = new Date(ele.value);
    var dd = susu.getDate();
    var mm = susu.getMonth() + 1; //January is 0!
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    pickedDate = susu.getFullYear() + '-' + mm + '-' + dd;
    $(ele.parentElement.nextElementSibling.children.todate).attr("min", pickedDate);
    $(ele.parentElement.nextElementSibling.children.todate).attr("value", pickedDate);
    };