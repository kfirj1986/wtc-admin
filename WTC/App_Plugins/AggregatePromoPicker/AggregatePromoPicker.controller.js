angular.module("umbraco")
    .controller("WTC.AggregatePromoPickerController",
    function ($scope, assetsService, notificationsService, $compile, $http) {
        //this function will execute when all dependencies have loaded
        //notificationsService.success("Success", "Speaker has been deleted");


        $scope.savepromotiongroup = function () {
            debugger;
            var pkgNumber = $('#mainpromoddl').val();
            if (!pkgNumber || pkgNumber == "undefined") {
                notificationsService.error("Error", "you must pick Promo");
                //alert('you must pick destination');
                return;
            }
            $scope.model.value = pkgNumber + " | " + $('#mainpromoddl option:selected').text();
        };

        $scope.mySplit = function (string, nb) {
            var array = string.split('|');
            return array[nb];
        }

        $scope.checkpromotion = function (ele) {
            if (!ele.target.parentElement.parentElement.children.promodasts.children.segmentddl.value) {
                notificationsService.error("Error", "you must pick segment");
                return;
            }

            $http.get("/umbraco/api/Main/GetSiteInfo")
                .then(function (response) {
                    if (response != null && response.data != null &&  response.data != "null") {
                        var apiaddress = response.data.appUrl + "site/aggregate-promotions/" + ele.target.parentElement.parentElement.children.promodasts.children.segmentddl.value;
                        apiaddress += "?APIKey=" + response.data.appAPIKey + "&lang = he-IL";
                        console.info(apiaddress);
                        $http.get(apiaddress)
                            .then(function (response) {
                                if (response != null && response.data != null && response.data.results != null) {
                                    response.data = response.data.results;
                                    if (response.data.length > 0) {
                                        response.data.sort(SortByName);
                                        var unLoadPackageMessage = "Promotion Count = " + response.data.length + " | ";
                                        var anew = "<div><label style='display:inline-block;width:10%'>choose promo:</label><select id='mainpromoddl'><option value=''>Choose</option>";
                                        $(response.data).each(function () {
                                            if (this.displayView != "aggregate-promotion") {
                                                return true;
                                            }
                                            try {
                                                var currentPromotion = "<option value='" + this.number + "'>" + this.name +"</option>";
                                                anew += currentPromotion;
                                            } catch (e) {
                                                console.error(e.message);
                                            }
                                        });
                                        anew += "</select> <input type='button' ng-click='viewpromotion()' class='btn btn-primary' value='View Promotions'/> <input type='button' ng-click='savepromotiongroup()' class='btn btn-primary' value='save Promotions Group'/> </div>";

                                        $scope.html = anew;
                                        notificationsService.info("Info", unLoadPackageMessage);
                                    }
                                    else {
                                        notificationsService.error("Info", "no promotions found for this search");
                                    }
                                }
                            });
                    }
                });
        };

        $scope.viewpromotion = function () {
            var promoCode = $('#mainpromoddl').val();
            if (!promoCode || promoCode == "undefined") {
                notificationsService.error("Error", "you must pick Promo");
                //alert('you must pick destination');
                return;
            }

            $http.get("/umbraco/api/Main/GetSiteInfo")
                .then(function (response) {
                    if (response != null && response.data != null) {
                        var apiaddress = response.data.appUrl + "site/promotions-aggregate/" + promoCode;
                        apiaddress += "?APIKey=" + response.data.appAPIKey + "&lang = he-IL";
                        console.info(apiaddress);
                        $http.get(apiaddress)
                            .then(function (response) {
                                debugger;
                                if (response != null && response.data != null && response.data.results != null) {
                                    response.data = response.data.results;
                                    if (response.data.length > 0) {
                                        response.data.sort(SortByName);
                                        var unLoadPackageMessage = "Promotion Count = " + response.data.length + " | ";
                                        var anew = "<div class='mygrid-wrapper-div'><div class='col-md-12 mt-15'>";
                                        $(response.data).each(function () {
                                            try {
                                                if (!this.media || !this.media.images) {
                                                    unLoadPackageMessage += this.title + " no hotel image! | ";
                                                }
                                                var currentPromotion = "";
                                                switch (this.segment) {
                                                    case "PACKAGES":
                                                    case "HOTELS":
                                                        currentPromotion = "<div style='width:45%;float:left;margin-right:10px;'>\
                                    <div class='card " + ($scope.checkIfSelected(this.packageNumber) ? 'choosen-promo' : '') + "'><img class='card-img-top' src='" + this.media.images[0].src + "' alt='Card image cap'>\
                                    <div class='card-block row'><h4 class='card-title text-center' style='color:#2e1414'>" + this.title + "</h4>\
                                    <div style='width:50%;float:left;'>\
                                    <p style='text-align:center'>" + this.destinationData.name + "</p>\
                                    <p style='text-align:center'>roomType: " + this.roomType.name + "</p>\
                                    <p style='text-align:center'>pkg number: " + this.packageNumber + "</p>\
                                    <p style='text-align:center'>pkg code: " + this.code.split('|')[1] + "</p>\
                                    <p style='text-align:center'>" + this.fromDate + "</p>\
                                    </div><div style='width:50%;float:left;'>\
                                    <p style='text-align:center' title='" + this.mainProductName + "'>" + $scope.cutLongString(this.mainProductName) + "</p>\
                                    <p style='text-align:center'>nights: " + this.nights + "</p>\
                                    <p style='text-align:center'>adults: " + this.composition.adults + " | children: " + this.composition.children + "</p>\
                                    <p style='text-align:center'>" + this.boardBase.code + " - " + this.boardBase.name + "</p>\
                                    <p style='text-align:center'>" + this.toDate + "</p>\
                                    </div>\
                                    <h4 class='card-title text-center' style='color:#2e1414'>" + this.price.currency.symbol + this.price.total + "</h4>\
                                    <div style='display:inline-block;width:100%'>\
                                    </div></div></div></div>";
                                                        break;
                                                    case "FLYANDDRIVE":
                                                        currentPromotion = "<div style='width:45%;float:left;margin-right:10px;'>\
                                    <div class='card " + ($scope.checkIfSelected(this.packageNumber) ? 'choosen-promo' : '') + "'><img class='card-img-top' src='" + this.media.images[0].src + "' alt='Card image cap'>\
                                    <div class='card-block row'><h4 class='card-title text-center' style='color:#2e1414'>" + this.title + "</h4>\
                                    <div style='width:50%;float:left;'>\
                                    <p style='text-align:center'>" + this.destinationData.name + "</p>\
                                    <p style='text-align:center'>pkg number: " + this.packageNumber + "</p>\
                                    <p style='text-align:center'>Airline: " + this.outBoundFlightLegs[0].flights[0].airline.name + "</p>\
                                    <p style='text-align:center'>" + this.fromDate + "</p>\
                                    </div><div style='width:50%;float:left;'>\
                                    <p style='text-align:center' title='" + this.mainProductName + "'>" + $scope.cutLongString(this.mainProductName) + "</p>\
                                    <p style='text-align:center'>adults: " + this.composition.adults + " | children: " + this.composition.children + "</p>\
                                    <p style='text-align:center'>" + this.toDate + "</p>\
                                    </div>\
                                    <h4 class='card-title text-center' style='color:#2e1414'>" + this.price.currency.symbol + this.price.total + "</h4>\
                                    <div style='display:inline-block;width:100%'>\
                                    </div></div></div></div>";
                                                        break;
                                                    case "FLIGHTS":
                                                        currentPromotion = "<div style='width:45%;float:left;margin-right:10px;'>\
                                    <div class='card " + ($scope.checkIfSelected(this.packageNumber) ? 'choosen-promo' : '') + "'><img class='card-img-top' src='" + this.media.images[0].src + "' alt='Card image cap'>\
                                    <div class='card-block row'><h4 class='card-title text-center' style='color:#2e1414'>" + this.title + "</h4>\
                                    <div style='width:50%;float:left;'>\
                                    <p style='text-align:center'>" + this.destinationData.name + "</p>\
                                    <p style='text-align:center'>pkg number: " + this.packageNumber + "</p>\
                                    <p style='text-align:center'>Airline: " + this.outBoundFlightLegs[0].flights[0].airline.name + "</p>\
                                    <p style='text-align:center'>" + this.fromDate + "</p>\
                                    </div><div style='width:50%;float:left;'>\
                                    <p style='text-align:center'>adults: " + this.composition.adults + " | children: " + this.composition.children + "</p>\
                                    <p style='text-align:center'>" + this.toDate + "</p>\
                                    </div>\
                                    <h4 class='card-title text-center' style='color:#2e1414'>" + this.price.currency.symbol + this.price.total + "</h4>\
                                    <div style='display:inline-block;width:100%'>\
                                    </div></div></div></div>";
                                                        break;
                                                    default:
                                                }

                                                anew += currentPromotion;
                                            } catch (e) {
                                                console.error(e.message);
                                            }
                                        });
                                        anew += "</div></div>";

                                        $scope.promolisthtml = anew;
                                        notificationsService.info("Info", unLoadPackageMessage);
                                    }
                                    else {
                                        notificationsService.error("Info", "no promotions found for this search");
                                    }
                                }
                            });

                    }
                });
        };

        $scope.cutLongString = function (str) {
            if (str.length > 15) {
                return str.substring(0, 14) + "...";
            }
            return str;
        };

        $scope.checkIfSelected = function (pkgNumber) {
            if (pkgNumber == $scope.model.value.split('|')[0]) {
                return true;//"choosen-promo";
            }
            return false;
        };



        //load the separate css for the editor to avoid it blocking our js loading
        //assetsService.loadCss("/App_Plugins/AggregatePromoPicker/lib/bootstrap.css");

    }).directive('dynamic', function ($compile) {
        return {
            restrict: 'A',
            replace: true,
            link: function (scope, ele, attrs) {
                scope.$watch(attrs.dynamic, function (html) {
                    ele.html(html);
                    $compile(ele.contents())(scope);
                });
            }
        };
    });

//This will sort your array
function SortByName(a, b) {
    if (a.name && b.name) {
        var aName = a.name.toLowerCase();
        var bName = b.name.toLowerCase();
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }
    else {
        return ((a.fromDate > b.fromDate) ? 1 : 0);
    }
    
}


