﻿using Examine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using umbraco;
using umbraco.cms.businesslogic.web;
using umbraco.NodeFactory;
using Umbraco.Core;
using Umbraco.Core.Services;

namespace WTC.App_Start
{
    public class CustomEventHandler : ApplicationEventHandler
    {
        //public static WTC.Models.SiteInfo GlobalSiteInfo { get; set; }
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        public CustomEventHandler()
        {
            ExamineManager.Instance.IndexProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetIndexer(false)].GatheringNodeData += ExamineCustomIndexes_GatheringNodeData;

            //Umbraco.Core.Services.ContentService.Creating += ContentService_Creating;

            Umbraco.Core.Services.ContentService.Saving += ContentServiceSaving;

            Umbraco.Core.Services.ContentService.Saved += ContentService_Saved;

            Umbraco.Core.Services.ContentService.Published += ContentService_Published;
        }

        #region Examine Indexing
        /// <summary>
        /// Build manual indexes for quick search by this index
        /// </summary>
        private void BuildHotelsIndex(Node indexingNode, ref IndexingNodeDataEventArgs e)
        {
            // here run logic to build indexes upon search requirements
            Node oNode = AddPropertyIndex(indexingNode, ref e, "city", "cityId");
            if (oNode != null)
            {
                if (oNode.Parent == null)
                    return;
                e.Fields.Add("countryId", oNode.Parent.Id.ToString());

                oNode = AddPropertyIndex(oNode, ref e, "area", "areaId");
            }
        }

        private void BuildTripDateIndex(Node indexingNode, ref IndexingNodeDataEventArgs e)
        {
            AddIndexObjectNameByID(indexingNode, ref e, "guide", "guideName");
        }

        private void BuildOrganizedTripIndex(Node indexingNode, ref IndexingNodeDataEventArgs e)
        {
            removeCommasFromTreePickerField(ref e, "tripDestinations");
        }

        private void BuildPromotionIndex(Node indexingNode, ref IndexingNodeDataEventArgs e)
        {
            removeCommasFromTreePickerField(ref e, "subjects");
            removeCommasFromTreePickerField(ref e, "destinations");

            //Get archetype to object for archetype field index
            string objectPickerArchetypeJson = indexingNode.GetProperty("objectPicker").Value;
            if (!string.IsNullOrEmpty(objectPickerArchetypeJson))
            {
                WTC.Models.Object_Picker object_Picker = WTC.Utils.JsonUtils.DeserializeJSON<WTC.Models.Object_Picker>(objectPickerArchetypeJson);
                if (object_Picker.fieldsets != null)
                {
                    var objectPick = object_Picker.fieldsets.FirstOrDefault();
                    if (objectPick != null)
                    {
                        switch (objectPick.alias)
                        {
                            case "hotelPicker":
                                e.Fields.Add("segment", ((int)WTC.Models.ESegment.hotel).ToString());
                                break;
                            case "packagePicker":
                                e.Fields.Add("segment", ((int)WTC.Models.ESegment.package).ToString());
                                break;
                            case "organizedTripPicker":
                                e.Fields.Add("segment", ((int)WTC.Models.ESegment.trip).ToString());
                                break;
                            case "freeLink":
                                e.Fields.Add("segment", "-1");
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        private void BuildDestinationsIndex(Node indexingNode, ref IndexingNodeDataEventArgs e)
        {
            AddIndexObjectNameByID(indexingNode, ref e, "area", "areaName");
        }

        private void BuildPromotionsIndex(Node indexingNode, ref IndexingNodeDataEventArgs e)
        {
            switch (indexingNode.NodeTypeAlias)
            {
                case "destinationPromotion":
                    break;
                case "generalPromotion":
                    break;
                case "hotelPromotion":
                    AddPropertyIndex(indexingNode, ref e, "hotel", "hotelId");
                    break;
                case "packagePromotion":
                    break;
                case "segmentPromotion":
                    break;
                case "tagPromotion":
                    break;
                case "tripPromotion":
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// הוספת אינדקס לאובייקט על בסיס שדות קיימים
        /// </summary>
        /// <param name="indexingNode">האובייקט שנשמר</param>
        /// <param name="e">אירוע השמירה</param>
        /// <param name="propertyAlias">מהו המשתנה (שקיים באובייקט) שאותו אנו רוצים לשמור</param>
        /// <param name="indexKey">מהו שם האינדקס שאנו רוצים </param>
        /// <returns></returns>
        private Node AddPropertyIndex(Node indexingNode, ref IndexingNodeDataEventArgs e, string propertyAlias, string indexKey)
        {
            if (!indexingNode.HasProperty(propertyAlias))
                return null;

            var propertyAliasId = indexingNode.GetProperty(propertyAlias).Value;
            if (propertyAliasId == null)
                return null;
            var propertyNode = uQuery.GetNode(propertyAliasId);
            if (propertyNode == null)
                return null;
            e.Fields.Add(indexKey, propertyNode.Id.ToString());
            return propertyNode;
        }


        /// <summary>
        /// הוספת אינדקס שם של אובייקט שמקושר באמצעות מזהה (על ידי קונטנט פיקר)
        /// </summary>
        /// <param name="indexingNode">האובייקט שנשמר</param>
        /// <param name="e"> e אירוע השמירה - לשלוח </param>
        /// <param name="propertyAlias">האליאס של המשתנה המקושר שמכיל את המזהה</param>
        /// <param name="indexKey">שם האינדקס החדש שיוצרים</param>
        /// <returns></returns>
        private Node AddIndexObjectNameByID(Node indexingNode, ref IndexingNodeDataEventArgs e, string propertyAlias, string indexName)
        {
            if (!indexingNode.HasProperty(propertyAlias))
                return null;

            var propertyAliasId = indexingNode.GetProperty(propertyAlias).Value;
            if (propertyAliasId == null)
                return null;
            var propertyNode = uQuery.GetNode(propertyAliasId);
            if (propertyNode == null)
                return null;
            e.Fields.Add(indexName, WTC.Utils.ContentUtils.GetNodeMainName(propertyNode, "mainName"));
            return propertyNode;
        }
        #endregion

        #region Custom Handlers
        //void ContentService_Creating(IContentService sender, Umbraco.Core.Events.NewEventArgs<Umbraco.Core.Models.IContent> e)
        //{
        //    if (e.Entity == null)
        //        return;

        //    if (e.Entity.ContentType == null)
        //        return;

        //    var parentNode = new Node(e.Entity.ParentId);

        //    if (parentNode != null)
        //    {
        //        if (parentNode.NodeTypeAlias == "organizeTrip" && e.Entity.ContentType.Alias == "tripDates")
        //        {
        //            if (parentNode.Children.Count > 0)
        //            {
        //                foreach (Node childNode in parentNode.Children)
        //                {
        //                    if (childNode.NodeTypeAlias == "tripDates")
        //                    {
        //                        e.Cancel = true;
        //                        e.CancelOperation(new Umbraco.Core.Events.EventMessage("Not Valied", "Only one dates folder allowed"));

        //                        e.Messages.Add(new Umbraco.Core.Events.EventMessage("Not Valied", "Only one dates folder allowed"));

        //                        //var clientTool = new umbraco.BasePages.ClientTools((System.Web.UI.Page)HttpContext.Current.CurrentHandler);
        //                        //clientTool.ShowSpeechBubble(Umbraco.Web.UI.SpeechBubbleIcon.Error, "Warning", "Only one dates folder allowed!");
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        void ExamineCustomIndexes_GatheringNodeData(object sender, IndexingNodeDataEventArgs e)
        {
            var indexingNode = uQuery.GetNode(e.NodeId);
            if (indexingNode == null)
                return;

            switch (indexingNode.NodeTypeAlias)
            {
                case "hotel":
                    {
                        BuildHotelsIndex(indexingNode, ref e);
                    }
                    break;
                case "tripDate":
                    {
                        BuildTripDateIndex(indexingNode, ref e);
                    }
                    break;
                case "organizedTrip":
                    {
                        BuildOrganizedTripIndex(indexingNode, ref e);
                    }
                    break;
                case "promotion":
                    {
                        BuildPromotionIndex(indexingNode, ref e);
                    }
                    break;
                case "continent":
                case "country":
                case "city":
                    {
                        BuildDestinationsIndex(indexingNode, ref e);
                    }
                    break;
                default:
                    if (indexingNode.Parent == null)
                    {

                    }
                    else if (indexingNode.Parent.Parent == null)
                    {
                        //Footer
                    }
                    else if (indexingNode.Parent.Parent.NodeTypeAlias == "lobbyPageTemplates")
                    {
                        //Lobby Pages Indexes
                    }
                    else if (indexingNode.Parent.Parent.NodeTypeAlias == "promotions")
                    {
                        BuildPromotionsIndex(indexingNode, ref e);
                    }
                    break;
            }

            #region Not Work
            //UmbracoHxelper oUmbracoHelper = new UmbracoHelper();
            //var indexingNode = oUmbracoHelper.Content(e.NodeId); 
            #endregion
        }

        void ContentService_Saved(IContentService sender, Umbraco.Core.Events.SaveEventArgs<Umbraco.Core.Models.IContent> e)
        {
            if (e.SavedEntities == null)
                return;

            if (e.SavedEntities.FirstOrDefault() == null)
                return;

            switch (e.SavedEntities.First().ContentType.Alias)
            {
                case "organizedTrip":
                    {
                        #region Create Default Object
                        createDefaultNodeForParentNode(e, "itineraries", "Itineraries");
                        createDefaultNodeForParentNode(e, "tripDates", "Dates");
                        #endregion
                    }
                    break;
                case "itineraries":
                    {
                        #region Create Default Object
                        //createDefaultNodeForParentNode(e, "itinerary", "Default itinerary");
                        #endregion
                    }
                    break;
                case "hotel":
                    {
                        createDefaultMediaFolderForNode(e, EMediaFolderType.Hotels);
                    }
                    break;
                case "siteInfo":
                    {
                        //return MapperUtils.Map<T>(searchResult.Fields, lang);
                        //GlobalSiteInfo = WTC.Utils.MapperUtils.Map<WTC.Models.SiteInfo>(e.SavedEntities.FirstOrDefault().Properties, "");
                        // e.SavedEntities.FirstOrDefault();
                    }
                    break;
                default:
                    break;
            }
        }
        void ContentServiceSaving(IContentService sender, Umbraco.Core.Events.SaveEventArgs<Umbraco.Core.Models.IContent> e)
        {
            var parentNode = new Node(e.SavedEntities.First().ParentId);
            switch (e.SavedEntities.First().ContentType.Alias)
            {
                case "tripDates":
                    if (parentNode != null)
                    {
                        if (checkIfDefaultNodeExist(parentNode, "tripDates"))
                        {
                            cancelOperation(e, "Not Valied", "Only one object of this type can be nested");
                            //e.Messages.Add(new Umbraco.Core.Events.EventMessage("Not Valied", "Only one dates folder allowed"));
                            //var clientTool = new Umbraco.Web.UI.Pages.ClientTools((System.Web.UI.Page)HttpContext.Current.CurrentHandler);
                            //clientTool.ShowSpeechBubble(Umbraco.Web.UI.SpeechBubbleIcon.Error, "Warning", "Only one dates folder allowed!");
                        }
                    }
                    break;
                case "itineraries":
                    if (parentNode != null)
                    {
                        if (checkIfDefaultNodeExist(parentNode, "itineraries"))
                        {
                            cancelOperation(e, "Not Valied", "Only one object of this type can be nested");
                        }
                    }
                    break;
                case "tripDate":
                    if (e.SavedEntities.First().IsValid())
                    {
                        if (Convert.ToDateTime(e.SavedEntities.First().GetValue("start")) > Convert.ToDateTime(e.SavedEntities.First().GetValue("end")))
                        {
                            e.Messages.Add(new Umbraco.Core.Events.EventMessage("Not Valied", "Start date must be bigger then end date!", Umbraco.Core.Events.EventMessageType.Error));
                            e.Cancel = true;
                            //cancelOperation(e, "Not Valied", "Start date must be bigger then end date");                                                        
                        }
                    }
                    break;
                case "promotion":
                    if (e.SavedEntities.First().IsValid())
                    {
                        if (Convert.ToDateTime(e.SavedEntities.First().GetValue("fromDate")) > Convert.ToDateTime(e.SavedEntities.First().GetValue("toDate")))
                        {
                            e.Messages.Add(new Umbraco.Core.Events.EventMessage("Not Valied", "Start date must be bigger then end date!", Umbraco.Core.Events.EventMessageType.Error));
                            e.Cancel = true;
                            //cancelOperation(e, "Not Valied", "Start date must be bigger then end date");                                                        
                        }
                    }
                    break;
                //case "confirmationEmailTemplate":
                //    if (e.SavedEntities.First().IsValid())
                //    {
                //        var html = e.SavedEntities.First().GetValue("mainLayout").ToString();
                //        e.SavedEntities.First().SetValue("mainLayout", HttpContext.Current.Server.HtmlEncode(html));                        
                //    }
                //    break;

                default:
                    break;
            }


        }

        void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {

            string ItemType = "";
            int ContentId = -1;

            var publishedNode = e.PublishedEntities.First();
            switch (publishedNode.ContentType.Alias)
            {
                #region Settings
                case "airline":
                case "boardBase":
                case "currency":
                case "hotelType":
                case "language":
                case "segment":
                case "stamp":
                case "galleryType":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                #region Subjects
                case "subject":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                #region Promotions
                case "promotion":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                #region Fly & Drive
                case "flyAndDriveCar":
                case "flyAnsDriveCarTransmission":
                case "flyAndDriveClass":
                case "flyAndDriveCarCategory":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                #region Destinations
                case "area":
                case "continent":
                case "country":
                case "city":
                case "airport":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                #region Organized Trip
                case "guide":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                case "organizedTrip":
                    ContentId = publishedNode.Id;
                    goto case "Tour";
                case "tripDate":
                case "itinerary":
                    ContentId = new Node(publishedNode.ParentId).Parent.Id;
                    goto case "Tour";
                case "itineraryDay":
                    ContentId = new Node(publishedNode.ParentId).Parent.Parent.Id;
                    goto case "Tour";
                case "Tour":
                    ItemType = "organizedTrip";
                    break;
                #endregion

                #region Hotel
                case "hotelChain":
                case "hotelFacility":
                case "roomFacility":
                case "hotel":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                #region Lobby Page
                case "destinationPage":
                case "generalPage":
                case "segmentInDestinationPage":
                case "segmentPage":
                case "subjectPage":
                    ContentId = publishedNode.Id;
                    ItemType = "LobbyPage";
                    break;
                #endregion

                #region Static Page
                case "aboutUs":
                case "contactUs":
                case "termsAndConditions":
                case "homePage":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                #region Menu & Footer
                case "menuItem":
                case "footer":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                #region Site Info
                case "siteInfo":
                    ContentId = publishedNode.Id;
                    ItemType = publishedNode.ContentType.Alias;
                    break;
                #endregion

                default:
                    break;
            }

            if (!string.IsNullOrEmpty(ItemType) && ContentId > 0)
            {

                WTC.Repository.StaticPagesRepository staticPagesRepository = new WTC.Repository.StaticPagesRepository();
                WTC.Models.SiteInfo siteInfo = staticPagesRepository.GetSiteInfo(WTC.Utils.VortoUtils.GetDefaultLang()).FirstOrDefault();
                if (siteInfo != null)
                {
                    Task.Factory.StartNew(async () =>
                    {
                        bool result = false;
                        try
                        {
                            int publisherDelay = 0;
                            if (!int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["ExportPublisherDelay"], out publisherDelay))
                            {
                                //Default - 5 sec if not declare
                                publisherDelay = 5000;
                            }
                            await Task.Delay(publisherDelay);

                            using (var client = new System.Net.Http.HttpClient())
                            {
                                client.BaseAddress = new Uri(siteInfo.appUrl);
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                                string path = string.Format("{0}/site/publisher/{1}/{2}/update", siteInfo.appAPIKey, ItemType, ContentId);

                                System.Net.Http.HttpResponseMessage response = await client.GetAsync(path);
                                if (response.IsSuccessStatusCode)
                                {
                                    var statusRes = response.Content.ReadAsStringAsync().Result;
                                    bool.TryParse(statusRes, out result);
                                }
                                //return result;
                            }
                        }
                        catch (Exception ex)
                        {
                            //return false;
                            //throw;
                            result = false;
                        }


                    });


                }


            }

        }
        #endregion

        #region Examine Extensions
        /// <summary>
        /// check If Default Node Exist Before Adding It
        /// </summary>
        /// <param name="node">The parent node</param>
        /// <param name="defaultNodeSearched">The default Node We Searching</param>
        /// <returns></returns>
        private static bool checkIfDefaultNodeExist(Node node, string defaultNodeSearched)
        {
            if (node.Children.Count > 0)
            {
                if (node.ChildrenAsList.FirstOrDefault(R => R.NodeTypeAlias == defaultNodeSearched) != null)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Remove Commas From multinode TreePicker Fields for search by one of the values
        /// </summary>
        /// <param name="e">IndexingNodeDataEventArgs e (send by ref)</param>
        /// <param name="fieldAlias">the existing multinode TreePicker Field alias</param>
        private static void removeCommasFromTreePickerField(ref IndexingNodeDataEventArgs e, string fieldAlias)
        {
            if (e.Fields.ContainsKey(fieldAlias))
            {
                //grab the current data from the Fields collection
                var indexList = e.Fields[fieldAlias];
                //let's get rid of those commas an put spaces insted
                indexList = indexList.Replace(",", " ");
                //now put it back into the Fields
                e.Fields[fieldAlias] = indexList;
            }
        }

        /// <summary>
        /// cancel Save Node Operation
        /// </summary>
        /// <param name="e">The Saved Event Entity</param>
        /// <param name="Category">The Message Title</param>
        /// <param name="Message">The Message Content</param>
        private static void cancelOperation(Umbraco.Core.Events.SaveEventArgs<Umbraco.Core.Models.IContent> e, string Category, string Message)
        {
            e.Cancel = true;
            e.CancelOperation(new Umbraco.Core.Events.EventMessage(Category, Message));
        }

        /// <summary>
        /// Create Default Node programmatically For Other Node (for example: creating Dates node [=date builder] for trip while creating trip)
        /// </summary>
        /// <param name="e">The Saved Event Entity</param>
        /// <param name="defaultNodeAlias">The new default node Alias (the one we want to create programmatically)</param>
        /// <param name="defaultNodeTitle"></param>
        private static void createDefaultNodeForParentNode(Umbraco.Core.Events.SaveEventArgs<Umbraco.Core.Models.IContent> e, string defaultNodeAlias, string defaultNodeTitle)
        {
            var node = new Node(e.SavedEntities.First().Id);
            if (node == null)
                return;

            if (checkIfDefaultNodeExist(node, defaultNodeAlias))
                return;

            // The umbraco user that should create the document, 
            // 0 is the umbraco system user, and always exists
            umbraco.BusinessLogic.User u = new umbraco.BusinessLogic.User(0);

            DocumentType documentType = DocumentType.GetByAlias(defaultNodeAlias);

            //Replace 1055 with id of parent node
            Document doc = Document.MakeNew(defaultNodeTitle, documentType, u, e.SavedEntities.First().Id);

            //after creating the document, prepare it for publishing 
            doc.Publish(u);

            //Tell umbraco to publish the document
            umbraco.library.UpdateDocumentCache(doc.Id);


            #region The Obsolete function is
            //Umbraco.Core.Models.ContentType te = new Umbraco.Core.Models.ContentType(e.SavedEntities.First().ContentType, "tripDate");
            //Umbraco.Core.Models.Content co = new Umbraco.Core.Models.Content("Dates", e.SavedEntities.First().Id, te);
            //Umbraco.Core.Services.ContentService oContentService = new ContentService(null, null, null, null, null, null, null);
            //oContentService.Publish(co);
            #endregion
        }

        /// <summary>
        /// Create Default Media folder programmatically For Objects (for example: creating hotel folder for contain all hotel images)
        /// </summary>
        /// <param name="e">The Saved Event Entity</param>
        /// <param name="mediaFolderType">The media folder type (from EMediaFolderType Enum)</param>
        private static void createDefaultMediaFolderForNode(Umbraco.Core.Events.SaveEventArgs<Umbraco.Core.Models.IContent> e, EMediaFolderType mediaFolderType)
        {
            //ApplicationContext.Current.Services.ContentService.CreateContent(name, parentId, contentTypeAlias, 0);
            if (e.SavedEntities.First().IsValid())
            {
                var parentMedia = ApplicationContext.Current.Services.MediaService.GetById((int)mediaFolderType);
                var newMedia = ApplicationContext.Current.Services.MediaService.CreateMedia(e.SavedEntities.First().Name, parentMedia, "Folder");

                List<Umbraco.Core.Models.IMedia> mediaList = ApplicationContext.Current.Services.MediaService.GetChildren(parentMedia.Id).ToList();
                if (mediaList.Any(M => M.Name.ToLower() == e.SavedEntities.First().Name.ToLower()) /*&& mediaList.FirstOrDefault(M => M.Name.ToLower() == e.SavedEntities.First().Name.ToLower()) != null*/)
                {
                    // התיקייה כבר קיימת או שקיימת תיקייה אחרת בשם זה
                }
                else
                {
                    //יצירת תיקיית מדיה עבור כל אובייקט חדש שנוצר במערכת
                    ApplicationContext.Current.Services.MediaService.Save(newMedia, 0, false);
                }
            }
        }

        public enum EMediaFolderType
        {
            Packages = 2950,
            Hotels = 2912,
            Tours = 2951,
            Destinations = 2911
        }
        #endregion

    }

}