﻿using Examine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using Umbraco.Web;

namespace WTC.Utils
{
    public class ExamineUtils
    {
        /// <summary>
        ///     Map examine search results collection to generic model collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="searchResults"></param>
        /// <returns></returns>
        public static IEnumerable<T> MapSearchResultsToModel<T>(IEnumerable<SearchResult> searchResults, string lang) where T : new()
        {
            return searchResults.Select(searchResult => searchResult.MapSearchResultItemToModel<T>(lang)).ToList();
        }

        /// <summary>
        ///     Map examine search result to generic  model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="searchResult"></param>
        /// <returns></returns>
        public static T MapSearcResultItemToModel<T>(SearchResult searchResult, string lang) where T : new()
        {
            return searchResult.MapSearchResultItemToModel<T>(lang);
        }

        public static Examine.SearchCriteria.IBooleanOperation GetQueryByNodeTypeAlias(string nodeTypeAlias, string id)
        {
            var searchProvider = ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
            var searchCriteria = searchProvider.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);
            Examine.SearchCriteria.IBooleanOperation query = searchCriteria.NodeTypeAlias(nodeTypeAlias);
            query.AddCondition("id", id);
            return query;
        }

        public static Examine.SearchCriteria.IBooleanOperation GetQueryByNodeId(string id)
        {
            var searchProvider = ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
            var searchCriteria = searchProvider.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);
            Examine.SearchCriteria.IBooleanOperation query = searchCriteria.Id(Convert.ToInt32(id));
            return query;
        }

        public static Examine.SearchCriteria.IBooleanOperation GetQueryByNodeIdList(string[] ids)
        {
            var searchProvider = ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
            var searchCriteria = searchProvider.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);
            Examine.SearchCriteria.IBooleanOperation query = searchCriteria.GroupedOr(new[] { "id" }, ids);
            return query;
        }

        public static Examine.SearchCriteria.IBooleanOperation GetQueryByNodeTypeAlias(string nodeTypeAlias, Dictionary<string, string> conditions)
        {
            var searchProvider = ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
            var searchCriteria = searchProvider.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);
            Examine.SearchCriteria.IBooleanOperation query = searchCriteria.NodeTypeAlias(nodeTypeAlias);
            query.AddConditions(conditions);

            return query;
        }


        public static Examine.SearchCriteria.IBooleanOperation GetGroupQueryByNodeTypeAliases(params string[] nodeTypeAliases)
        {
            var searchProvider = ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
            var searchCriteria = searchProvider.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);
            Examine.SearchCriteria.IBooleanOperation query = searchCriteria.GroupedOr(new[] { "nodeTypeAlias" }, nodeTypeAliases);
            return query;
        }


        public static Examine.SearchCriteria.IBooleanOperation GetGroupQueryByNodeTypeAliasesAndId(int id, params string[] nodeTypeAliases)
        {
            var searchProvider = ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
            var searchCriteria = searchProvider.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content);
            Examine.SearchCriteria.IBooleanOperation query = searchCriteria.GroupedOr(new[] { "nodeTypeAlias" }, nodeTypeAliases);
            query.AddCondition("id", id.ToString());
            return query;
        }


    }

    /// <summary>
    ///     Examine extension methods
    /// </summary>
    public static class ExamineUtilsExtensionMethods
    {
        /// <summary>
        ///     Safe way to retrieve field value
        /// </summary>
        /// <param name="searchResult"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static T GetFieldValue<T>(this SearchResult searchResult, string fieldName, T defaultValue = default(T))
        {
            if (searchResult.Fields.ContainsKey(fieldName))
            {
                return ParseUtils.Convert<T>(searchResult.Fields[fieldName]);
            }
            return defaultValue;
        }

        /// <summary>
        ///     Retrieve media examine search result media
        /// </summary>
        /// <param name="searchResult"></param>
        /// <param name="imageFieldName"></param>
        /// <param name="defaultUrl"></param>
        /// <returns></returns>
        public static string GetImageUrl(this SearchResult searchResult, string imageFieldName, string defaultUrl = "")
        {
            var imageId = GetFieldValue(searchResult, imageFieldName, 0);
            if (imageId == 0)
                return defaultUrl;

            var helper = new UmbracoHelper(UmbracoContext.Current);

            var mediaPicker = helper.TypedMedia(imageId);

            if (mediaPicker != null && mediaPicker.HasValue("umbracoFile"))
            {
                return mediaPicker.Url;
            }
            return defaultUrl;
        }

        /// <summary>
        ///     Highligh search term
        /// </summary>
        /// <param name="searchContent"></param>
        /// <param name="highlightHtmlFormat"></param>
        /// <param name="termsArray"></param>
        /// <returns></returns>
        public static T MapSearchResultItemToModel<T>(this SearchResult searchResult, string lang) where T : new()
        {
            return MapperUtils.Map<T>(searchResult.Fields, lang);
        }

        /// <summary>
        ///     Highlight html search results
        /// </summary>
        /// <param name="searchContent"></param>
        /// <param name="termsArray"></param>
        /// <param name="highlightHtmlFormat"></param>
        /// <returns></returns>
        public static HtmlString HighLightSearchTerm(this string searchContent, string[] termsArray, string highlightHtmlFormat = "<b style=\"font-size:larger;\">{0}</b>")
        {
            foreach (var term in termsArray)
            {
                searchContent = Regex.Replace(searchContent, term, string.Format(highlightHtmlFormat, term), RegexOptions.IgnoreCase);
            }
            return new HtmlString(searchContent);
        }

        public static Examine.SearchCriteria.IBooleanOperation AddCondition(this Examine.SearchCriteria.IBooleanOperation query, string fieldName, string fieldValue)
        {
            if (!string.IsNullOrEmpty(fieldValue))
            {
                query = query.And()
                .Field(fieldName, fieldValue);


                //query = query.And().GroupedOr(.Field(fieldName, fieldValue);
            }
            return query;
        }

        public static Examine.SearchCriteria.IBooleanOperation AddCondition(this Examine.SearchCriteria.IBooleanOperation query, KeyValuePair<string, string> condition)
        {

            return query.AddCondition(condition.Key, condition.Value);

        }

        public static Examine.SearchCriteria.IBooleanOperation AddConditions(this Examine.SearchCriteria.IBooleanOperation query, Dictionary<string, string> conditions)
        {
            if (conditions != null)
            {
                foreach (var condition in conditions)
                {
                    query.AddCondition(condition);
                }
            }
            return query;
        }


        public static string GetIndexer(bool onlyPublish)
        {
            return WebConfigurationManager.AppSettings[onlyPublish ? "PublishIndexer" : "UnPublishIndexer"].ToString();
        }

        public static string GetSearcher(bool onlyPublish)
        {
            //return "InternalSearcher";
            return WebConfigurationManager.AppSettings[onlyPublish ? "PublishSearcher" : "UnPublishSearcher"].ToString();
        }


    }
}