﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Umbraco.Web;

namespace WTC.Utils
{
    public class MapperUtils
    {
        public static T Map<T>(IDictionary<string, string> sourceFieldsToMap, string lang) where T : new()
        {
            var resultModel = new T();
            if (sourceFieldsToMap == null || !sourceFieldsToMap.Any())
                return resultModel;
            foreach (var propertyInfo in ReflectionUtils.GetPropertiesInfo<T>())
            {
                try
                {
                    var fieldName = propertyInfo.Name.ToLower();
                    var customAttributeData = ReflectionUtils.GetCustomAttributeData<CustomMapperField>(propertyInfo);
                    if (customAttributeData != null && !string.IsNullOrEmpty(customAttributeData.FieldAlias))
                        fieldName = customAttributeData.FieldAlias.ToLower();

                    var propertyType = propertyInfo.PropertyType;
                    if (sourceFieldsToMap.Keys.Any(x => x.ToLower() == fieldName))
                    {
                        var sourceField = sourceFieldsToMap.Single(x => x.Key.ToLower() == fieldName);
                        MapPropertyByAttribute<T>(sourceField.Value, lang, resultModel, propertyInfo, fieldName, customAttributeData, propertyType);
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.Enum)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapEnumToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(ReflectionUtils.GetEnumType(propertyType.FullName));
                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { sourceFieldsToMap.Single(x => x.Key == "nodeTypeAlias").Value }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.Inherit)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapInheritClassToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(propertyType);
                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { sourceFieldsToMap, lang }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.RelatedObject)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapRelatedObjectToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(propertyType);

                        var obj = sourceFieldsToMap.SingleOrDefault(x => x.Key.ToLower() == fieldName.Replace("object", ""));
                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { !string.IsNullOrEmpty(obj.Value) ? obj.Value : "", lang }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.SplitedText)
                    {

                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.Image)
                    {
                        //var method = typeof(MapperUtils).GetMethod("MapImageToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        //propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { value, lang }));


                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapImageToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var obj = sourceFieldsToMap.SingleOrDefault(x => x.Key.ToLower() == fieldName);
                        propertyInfo.SetValue(resultModel, mapTypeMethod.Invoke(null, new object[] { !string.IsNullOrEmpty(obj.Value) ? obj.Value : "", lang }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.RelatedObjectList)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapRelatedObjectListToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(propertyType);

                        Type itemType = null;
                        if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition()
                                == typeof(List<>))
                        {
                            itemType = propertyType.GetGenericArguments()[0]; // use this...
                        }
                        propGeneric = mapTypeMethod.MakeGenericMethod(itemType);

                        var obj = sourceFieldsToMap.SingleOrDefault(x => x.Key.ToLower() == fieldName.Replace("object", ""));
                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { !string.IsNullOrEmpty(obj.Key) ? obj.Key : "", !string.IsNullOrEmpty(obj.Value) ? obj.Value : "", lang }));
                    }
                    else if (fieldName.ToLower() == "alias")
                    {
                        propertyInfo.SetValue(resultModel, sourceFieldsToMap.Single(x => x.Key == "nodeTypeAlias").Value);
                    }
                    else if (fieldName.ToLower() == "id")
                    {
                        propertyInfo.SetValue(resultModel, sourceFieldsToMap.Single(x => x.Key == "id").Value);
                    }
                }
                catch (Exception ex)
                {
                    //Write the excption
                    propertyInfo.SetValue(resultModel, string.Format("Exception Message: {0} | Exception InnerException: {1}", ex.Message, ex.InnerException));
                }
            }

            return resultModel;
        }
        public static T Map<T>(umbraco.NodeFactory.Node node, string lang) where T : new()
        {
            var resultModel = new T();
            if (node == null || node.Properties == null || (node.Properties.Count == 0 && node.Children == null))
                return resultModel;

            foreach (var propertyInfo in ReflectionUtils.GetPropertiesInfo<T>())
            {
                try
                {
                    var fieldName = propertyInfo.Name.ToLower();
                    var customAttributeData = ReflectionUtils.GetCustomAttributeData<CustomMapperField>(propertyInfo);
                    if (customAttributeData != null && !string.IsNullOrEmpty(customAttributeData.FieldAlias))
                        fieldName = customAttributeData.FieldAlias.ToLower();

                    var propertyType = propertyInfo.PropertyType;
                    if (node.PropertiesAsList.Exists(x => x.Alias.ToLower() == fieldName))
                    {
                        MapPropertyByAttribute<T>(node.PropertiesAsList.Single(x => x.Alias.ToLower() == fieldName).Value, lang, resultModel, propertyInfo, fieldName, customAttributeData, propertyType);
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.Enum)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapEnumToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(ReflectionUtils.GetEnumType(propertyType.FullName));
                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { node.NodeTypeAlias }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.Childs)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapChildsToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(propertyType.GetGenericArguments().First());
                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { node.Children, fieldName, lang }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.RelatedObject)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapRelatedObjectToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(propertyType);

                        var obj = node.PropertiesAsList.SingleOrDefault(x => x.Alias.ToLower() == fieldName.Replace("object", ""));

                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { obj != null ? obj.Value : "", lang }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.SplitedText)
                    {

                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.RelatedObjectList)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapRelatedObjectListToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(propertyType);

                        Type itemType = null;
                        if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition()
                                == typeof(List<>))
                        {
                            itemType = propertyType.GetGenericArguments()[0]; // use this...
                        }
                        propGeneric = mapTypeMethod.MakeGenericMethod(itemType);

                        var obj = node.PropertiesAsList.SingleOrDefault(x => x.Alias.ToLower() == fieldName.Replace("object", ""));
                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { obj != null ? obj.Alias : "", obj != null ? obj.Value : "", lang }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.Date)
                    {
                        var method = typeof(MapperUtils).GetMethod("MapDateToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { fieldName }));
                    }
                    else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.Inherit)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapInheritClassToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(propertyType);
                        propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { node.PropertiesAsList.ToList().ToDictionary(g => g.Alias, g => g.Value, StringComparer.OrdinalIgnoreCase), lang }));
                    }

                    //else if (customAttributeData != null && customAttributeData.FieldType == CustomFieldType.CustomIndex)
                    //{
                    //    var mapTypeMethod = typeof(MapperUtils).GetMethod("MapCustomIndexToModel", BindingFlags.NonPublic | BindingFlags.Static);
                    //    var propGeneric = mapTypeMethod.MakeGenericMethod(propertyType.GetGenericArguments().First());
                    //    propertyInfo.SetValue(resultModel, propGeneric.Invoke(null, new object[] { node, fieldName, lang }));
                    //}
                    else if (fieldName == "id")
                    {
                        propertyInfo.SetValue(resultModel, node.Id.ToString());
                    }
                    else if (fieldName.ToLower() == "alias")
                    {
                        propertyInfo.SetValue(resultModel, node.NodeTypeAlias.ToString());
                    }
                    else
                    {
                        /*prop not mapped*/
                    }
                }
                catch (Exception ex)
                {
                    //Write the excption
                    propertyInfo.SetValue(resultModel, string.Format("Exception Message: {0} | Exception InnerException: {1}", ex.Message, ex.InnerException));
                }
            }

            return resultModel;
        }

        private static void MapPropertyByAttribute<T>(string value, string lang, T resultModel, PropertyInfo propertyInfo, string fieldName, CustomMapperField customAttributeData, Type propertyType) where T : new()
        {
            if (!ReflectionUtils.IsPropertyHasAttribute(propertyInfo, typeof(IgnoreMapper))) // skip mapping
            {
                try
                {
                    if (customAttributeData == null || (int)customAttributeData.FieldType == 0) // default mapper
                    {
                        var method = typeof(ParseUtils).GetMethod("Convert");
                        var generic = method.MakeGenericMethod(propertyType);
                        propertyInfo.SetValue(resultModel, generic.Invoke(null, new object[] { value }));
                    }
                    else // custom model mappers
                    {
                        if (customAttributeData.FieldType == CustomFieldType.ArchetypeField) //archetype mapper
                        {
                            var archeTypeMethod = typeof(MapperUtils).GetMethod("MapArchetypeJsonToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            var archeTypeGeneric = archeTypeMethod.MakeGenericMethod(propertyType.GetGenericArguments().First());
                            propertyInfo.SetValue(resultModel, archeTypeGeneric.Invoke(null, new object[] { (value), lang }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.MultipleTextString) // multiple textstring to collection mapper
                        {
                            var method = typeof(MapperUtils).GetMethod("MapMultipleTextStringFieldToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { value }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.CsvField) // csv field to collection mapper
                        {
                            var method = typeof(MapperUtils).GetMethod("MapCsvFieldToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            var generic = method.MakeGenericMethod(propertyType.GetGenericArguments().First());
                            propertyInfo.SetValue(resultModel, generic.Invoke(null, new object[] { value }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.JsonField)
                        {
                            var method = typeof(MapperUtils).GetMethod("MapJsonToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            var generic = method.MakeGenericMethod(propertyType);
                            propertyInfo.SetValue(resultModel, generic.Invoke(null, new object[] { value }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.VostroTranslated)
                        {
                            var method = typeof(MapperUtils).GetMethod("MapVostroTranslatedToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { value, lang }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.Image)
                        {
                            var method = typeof(MapperUtils).GetMethod("MapImageToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { value, lang }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.ImageGallery)
                        {
                            var method = typeof(MapperUtils).GetMethod("MapImageGalleryToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { value, lang }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.HTML)
                        {
                            var method = typeof(MapperUtils).GetMethod("MapHTMLStringToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { value }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.VostroTranslatedHTML)
                        {
                            var method = typeof(MapperUtils).GetMethod("MapVostroTranslatedHTMLStringToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { value, lang }));
                        }
                        else if (customAttributeData.FieldType == CustomFieldType.Date)
                        {
                            var method = typeof(MapperUtils).GetMethod("MapDateToModel", BindingFlags.NonPublic | BindingFlags.Static);
                            propertyInfo.SetValue(resultModel, method.Invoke(null, new object[] { value }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    propertyInfo.SetValue(resultModel, string.Format("Exception Message: {0} | Exception InnerException: {1}", ex.Message, ex.InnerException));
                }
            }
        }

        #region Mappers


        private static string MapVostroTranslatedHTMLStringToModel(string json, string lang)
        {

            string resultCollection = "";
            if (!string.IsNullOrEmpty(json))
            {
                var vostroModel = JsonUtils.DeserializeJSON<WTC.Utils.VortoUtils.VortoResult>(json);
                if (string.IsNullOrEmpty(lang))
                    lang = VortoUtils.GetDefaultLang();

                if (vostroModel.Values != null && vostroModel.Values.ContainsKey(lang))
                {
                    return HttpContext.Current.Server.HtmlEncode(vostroModel.Values.Single(x => x.Key == lang).Value);
                }
            }
            return resultCollection;

        }

        private static int MapEnumToModel<T>(string alias) where T : new()
        {
            int resultCollection = -1;
            if (!string.IsNullOrEmpty(alias))
            {
                resultCollection = (int)Enum.Parse(typeof(T), alias);
            }
            return resultCollection;
        }
        private static IEnumerable<T> MapChildsToModel<T>(umbraco.NodeFactory.Nodes childs, string fieldName, string lang) where T : new()
        {
            var resultCollection = new List<T>();
            foreach (umbraco.NodeFactory.Node child in childs)
            {
                if (fieldName == child.NodeTypeAlias.ToLower())
                {
                    var itemMapped = Map<T>(child, lang);
                    if (itemMapped != null)
                    {
                        resultCollection.Add(itemMapped);
                    }
                }
            }
            return resultCollection.Count == 0 ? null : resultCollection;
        }

        private static T MapRelatedObjectToModel<T>(string fieldValue, string lang) where T : new()
        {
            var result = new T();

            if (!string.IsNullOrEmpty(fieldValue))
            {
                Examine.SearchCriteria.IBooleanOperation query = ExamineUtils.GetQueryByNodeId(fieldValue);
                Examine.Providers.BaseSearchProvider ExamineSearcherProvider = Examine.ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
                var searchResults = ExamineSearcherProvider.Search(query.Compile());
                if (searchResults != null)
                {
                    if (searchResults.FirstOrDefault() != null)
                    {
                        result = Utils.ExamineUtils.MapSearcResultItemToModel<T>(searchResults.FirstOrDefault(), lang);
                    }
                }

                //umbraco.NodeFactory.Node newNode = umbraco.uQuery.GetNode(fieldValue);
                //if (newNode != null)
                //{
                //    var itemMapped = Map<T>(newNode, lang);
                //    if (itemMapped != null)
                //    {
                //        result = itemMapped;
                //    }
                //}
            }
            return result;
        }



        private static string MapSplitedTextToModel(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return str.Split('|')[0];
            }
            return str;
        }


        private static List<T> MapRelatedObjectListToModel<T>(string fieldAlias, string fieldValue, string lang) where T : new()
        {
            //return null;
            var result = new List<T>();

            if (!string.IsNullOrEmpty(fieldValue))
            {
                string[] idsArr = fieldValue.Split(',');

                Examine.SearchCriteria.IBooleanOperation query = ExamineUtils.GetQueryByNodeIdList(idsArr);
                Examine.Providers.BaseSearchProvider ExamineSearcherProvider = Examine.ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
                var searchResults = ExamineSearcherProvider.Search(query.Compile());
                if (searchResults != null)
                {
                    result.AddRange(Utils.ExamineUtils.MapSearchResultsToModel<T>(searchResults, lang).ToList());
                }
                //return null;


                //umbraco.NodeFactory.Node newNode = umbraco.uQuery.GetNode(id);
                //if (newNode != null)
                //{
                //    var itemMapped = Map<T>(newNode, lang);
                //    if (itemMapped != null)
                //    {
                //        result.Add(itemMapped);
                //    }
                //}

            }


            return result;
        }


        private static string MapHTMLStringToModel(string html)
        {
            string resultCollection = "";
            if (!string.IsNullOrEmpty(html))
            {
                resultCollection = HttpContext.Current.Server.HtmlEncode(html);

            }
            return resultCollection;
        }

        private static string MapDateToModel(string date)
        {
            string resultCollection = "";
            if (!string.IsNullOrEmpty(date))
            {
                DateTime dt = DateTime.MinValue;
                DateTime.TryParse(date, out dt);//Convert.ToDateTime(date);
                if (dt != null && dt > DateTime.MinValue)
                {
                    resultCollection = dt.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings["DateFormat"].ToString());
                }
            }
            return resultCollection;
        }

        private static string MapCustomIndexToModel(umbraco.NodeFactory.Node node, string CustomIndex, string lang)
        {
            string resultCollection = "";
            if (!string.IsNullOrEmpty(CustomIndex))
            {
                resultCollection = HttpContext.Current.Server.HtmlEncode(CustomIndex);

            }
            return resultCollection;
        }

        private static T MapInheritClassToModel<T>(IDictionary<string, string> sourceFieldsToMap, string lang) where T : new()
        {
            T resultClass = new T();
            resultClass = Map<T>(sourceFieldsToMap, lang);
            return resultClass;
        }

        private static T MapInheritArcheTypeClassToModel<T>(ArchetypeFieldsetModel archeRow, string lang) where T : new()
        {
            T resultClass = new T();
            foreach (var propertyInfo in ReflectionUtils.GetPropertiesInfo<T>())
            {
                if (ReflectionUtils.IsPropertyHasAttribute(propertyInfo, typeof(IgnoreMapper)))
                    continue;
                var customMapperFieldData = ReflectionUtils.GetCustomAttributeData<CustomMapperField>(propertyInfo);
                MapArcherowToModel<T>(lang, archeRow, resultClass, propertyInfo, customMapperFieldData);
            }
            return resultClass;
        }


        //private static string MapImageToModel(string imageId)
        //{
        //    string resultCollection = "";
        //    if (!string.IsNullOrEmpty(imageId))
        //    {
        //        var helper = new UmbracoHelper(UmbracoContext.Current);

        //        var mediaPicker = helper.TypedMedia(imageId);

        //        if (mediaPicker != null && mediaPicker.HasValue("umbracoFile"))
        //        {
        //            return string.Format("{0}{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["SiteBaseURL"].ToString(), mediaPicker.Url);
        //        }

        //    }
        //    return resultCollection;
        //}

        private static WTC.Models.Image MapImageToModel(string imageId, string lang)
        {
            WTC.Models.Image img = new Models.Image();
            if (!string.IsNullOrEmpty(imageId))
            {
                #region TEST
                //Examine.SearchCriteria.IBooleanOperation query = ExamineUtils.GetQueryByNodeId(imageId);
                var criteria = Examine.ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)].CreateSearchCriteria("media");
                var filter = criteria.Id(Convert.ToInt32(imageId));
                var results = Examine.ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)].Search(filter.Compile());
                if (results.Any())
                {
                    var mediaPicker = results.First();
                    if (mediaPicker != null)
                    {
                        if (mediaPicker.GetFieldValue<string>("umbracoFile") != null)
                        {
                            string json = mediaPicker.GetFieldValue<string>("umbracoFile").ToString();
                            if (!string.IsNullOrEmpty(json))
                            {
                                var imgModel = JsonUtils.DeserializeJSON<WTC.Utils.MediaResult>(json);
                                if (!string.IsNullOrEmpty(imgModel.src))
                                {
                                    img.src = string.Format("{0}{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["SiteBaseURL"].ToString(), imgModel.src);
                                }

                            }
                            //img.src = string.Format("{0}{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["SiteBaseURL"].ToString(), MapVostroTranslatedToModel(, lang));

                            if (!string.IsNullOrEmpty(mediaPicker.GetFieldValue<string>("alt")))
                            {
                                img.alt = MapVostroTranslatedToModel(mediaPicker.GetFieldValue<string>("alt").ToString(), lang);
                            }

                            if (!string.IsNullOrEmpty(mediaPicker.GetFieldValue<string>("title")))
                            {
                                img.title = MapVostroTranslatedToModel(mediaPicker.GetFieldValue<string>("alt").ToString(), lang);
                            }

                            return img;   
                        }
                    }
                }
                #endregion

                //var helper = new UmbracoHelper(UmbracoContext.Current);

                //var mediaPicker = helper.TypedMedia(imageId);

                //if (mediaPicker != null && mediaPicker.HasValue("umbracoFile"))
                //{

                //    img.src = string.Format("{0}{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["SiteBaseURL"].ToString(), mediaPicker.Url);

                //    if (mediaPicker.HasValue("alt"))
                //    {
                //        img.alt = MapVostroTranslatedToModel(mediaPicker.GetProperty("alt").DataValue.ToString(), lang);
                //    }

                //    if (mediaPicker.HasValue("title"))
                //    {
                //        img.title = MapVostroTranslatedToModel(mediaPicker.GetProperty("title").DataValue.ToString(), lang);
                //    }

                //    return img;
                //}
            }

            return GetNoImage(lang);
        }

        private static WTC.Models.Image GetNoImage(string lang)
        {
            return new Models.Image() { 
            alt = "No Image",
            title = "No Image",
            src = string.Format("{0}{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["SiteBaseURL"].ToString(), System.Web.Configuration.WebConfigurationManager.AppSettings["NoImageURL"].ToString())
            };
            //return null;
            //WTC.Models.Image img = new Models.Image();
            //WTC.Repository.StaticPagesRepository staticPagesRepository = new WTC.Repository.StaticPagesRepository();
            //WTC.Models.SiteInfo siteInfo = staticPagesRepository.GetSiteInfo(lang).FirstOrDefault();
            //if (siteInfo != null)
            //{
            //    if (siteInfo.noImage != null)
            //    {
            //        img.src = siteInfo.noImage.src;
            //        img.alt = siteInfo.noImage.alt;
            //        img.title = siteInfo.noImage.title;
            //    }
            //}
            //return img;
        }

        private static List<WTC.Models.Image> MapImageGalleryToModel(string imageIds, string lang)
        {
            List<WTC.Models.Image> resultCollection = new List<WTC.Models.Image>();
            if (!string.IsNullOrEmpty(imageIds))
            {
                var helper = new UmbracoHelper(UmbracoContext.Current);

                List<string> images = imageIds.Split(',').ToList();
                foreach (var item in images)
                {
                    resultCollection.Add(MapImageToModel(item, lang));
                }
            }
            return resultCollection;
        }

        private static string MapVostroTranslatedToModel(string json, string lang)
        {
            string resultCollection = "";
            if (!string.IsNullOrEmpty(json))
            {
                var vostroModel = JsonUtils.DeserializeJSON<WTC.Utils.VortoUtils.VortoResult>(json);
                if (string.IsNullOrEmpty(lang))
                    lang = VortoUtils.GetDefaultLang();

                if (vostroModel.Values != null && vostroModel.Values.ContainsKey(lang))
                {
                    //return text no HTML tags:
                    //return System.Text.RegularExpressions.Regex.Replace(vostroModel.Values.Single(x => x.Key == lang).Value, @"<[^>]+>|&nbsp;", "").Trim();
                    return vostroModel.Values.Single(x => x.Key == lang).Value;
                }
            }
            return resultCollection;
        }

        private static IEnumerable<T> MapArchetypeJsonToModel<T>(string json, string lang) where T : new()
        {
            IEnumerable<T> resultCollection = null;
            if (!string.IsNullOrEmpty(json))
            {
                var archeTypeModel = JsonUtils.DeserializeJSON<ArchetypeModel>(json);
                resultCollection = MapArchetypeToModel<T>(archeTypeModel, lang);
            }
            return resultCollection;
        }

        private static IEnumerable<T> MapArchetypeToModel<T>(ArchetypeModel archetypeModel, string lang) where T : new()
        {
            var resultCollection = new List<T>();
            if (archetypeModel == null)
                return default(IEnumerable<T>);

            foreach (var archeRow in archetypeModel.Fieldsets)
            {
                if (!archeRow.Disabled)
                {
                    // create result instance
                    var resultSingleItem = new T();
                    foreach (var propertyInfo in ReflectionUtils.GetPropertiesInfo<T>())
                    {
                        if (ReflectionUtils.IsPropertyHasAttribute(propertyInfo, typeof(IgnoreMapper)))
                            continue;
                        var customMapperFieldData = ReflectionUtils.GetCustomAttributeData<CustomMapperField>(propertyInfo);
                        MapArcherowToModel<T>(lang, archeRow, resultSingleItem, propertyInfo, customMapperFieldData);
                    }

                    //attach result single item to archerow
                    resultCollection.Add(resultSingleItem);
                }
            }
            return resultCollection;
        }

        private static void MapArcherowToModel<T>(string lang, ArchetypeFieldsetModel archeRow, T resultSingleItem, PropertyInfo propertyInfo, CustomMapperField customMapperFieldData) where T : new()
        {
            var archeRowProperty = archeRow.Properties.SingleOrDefault(x => x.Alias.ToLower() == propertyInfo.Name.ToLower());
            if (archeRowProperty != null && archeRowProperty.Value != null)
            {
                if (customMapperFieldData != null)
                {

                    if (customMapperFieldData.FieldType == CustomFieldType.JsonField)
                    {
                        var method = typeof(MapperUtils).GetMethod("MapJsonToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var generic = method.MakeGenericMethod(propertyInfo.PropertyType);
                        propertyInfo.SetValue(resultSingleItem, generic.Invoke(null, new object[] { archeRowProperty.Value.ToString() }));
                    }
                    else if (customMapperFieldData != null && customMapperFieldData.FieldType == CustomFieldType.Enum)
                    {
                        var mapTypeMethod = typeof(MapperUtils).GetMethod("MapEnumToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        var propGeneric = mapTypeMethod.MakeGenericMethod(ReflectionUtils.GetEnumType(propertyInfo.PropertyType.FullName));
                        propertyInfo.SetValue(resultSingleItem, propGeneric.Invoke(null, new object[] { archeRowProperty.Value }));
                    }
                    else if (customMapperFieldData.FieldType == CustomFieldType.ArchetypeField)
                    {
                        var archeTypeMethod = typeof(MapperUtils).GetMethod("MapArchetypeJsonToModel", BindingFlags.NonPublic | BindingFlags.Static);

                        var propertyType = propertyInfo.PropertyType;

                        var archeTypeGeneric = archeTypeMethod.MakeGenericMethod(propertyType.GetGenericArguments().First());
                        propertyInfo.SetValue(resultSingleItem, archeTypeGeneric.Invoke(null, new object[] { archeRowProperty.Value, lang }));
                    }
                    else if (customMapperFieldData.FieldType == CustomFieldType.Image)
                    {
                        var method = typeof(MapperUtils).GetMethod("MapImageToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        propertyInfo.SetValue(resultSingleItem, method.Invoke(null, new object[] { archeRowProperty.Value, lang }));
                    }
                    else if (customMapperFieldData.FieldType == CustomFieldType.ImageGallery)
                    {
                        var method = typeof(MapperUtils).GetMethod("MapImageGalleryToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        propertyInfo.SetValue(resultSingleItem, method.Invoke(null, new object[] { archeRowProperty.Value, lang }));
                    }
                    else if (customMapperFieldData.FieldType == CustomFieldType.VostroTranslated)
                    {
                        var method = typeof(MapperUtils).GetMethod("MapVostroTranslatedToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        propertyInfo.SetValue(resultSingleItem, method.Invoke(null, new object[] { archeRowProperty.Value, lang }));
                    }
                    else if (customMapperFieldData.FieldType == CustomFieldType.VostroTranslatedHTML)
                    {
                        var method = typeof(MapperUtils).GetMethod("MapVostroTranslatedHTMLStringToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        propertyInfo.SetValue(resultSingleItem, method.Invoke(null, new object[] { archeRowProperty.Value, lang }));
                    }
                    else if (customMapperFieldData.FieldType == CustomFieldType.Date)
                    {
                        var method = typeof(MapperUtils).GetMethod("MapDateToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        propertyInfo.SetValue(resultSingleItem, method.Invoke(null, new object[] { archeRowProperty.Value }));
                    }
                    else if (customMapperFieldData.FieldType == CustomFieldType.SplitedText)
                    {
                        var method = typeof(MapperUtils).GetMethod("MapSplitedTextToModel", BindingFlags.NonPublic | BindingFlags.Static);
                        propertyInfo.SetValue(resultSingleItem, method.Invoke(null, new object[] { archeRowProperty.Value }));
                    }
                }
                else
                {
                    propertyInfo.SetValue(resultSingleItem, archeRowProperty.Value.ToString());
                }
            }
            else if (customMapperFieldData != null && customMapperFieldData.FieldType == CustomFieldType.RelatedObject)
            {
                var mapTypeMethod = typeof(MapperUtils).GetMethod("MapRelatedObjectToModel", BindingFlags.NonPublic | BindingFlags.Static);
                var propGeneric = mapTypeMethod.MakeGenericMethod(propertyInfo.PropertyType);
                var obj = archeRow.Properties.SingleOrDefault(x => x.Alias.ToLower() == propertyInfo.Name.ToLower().Replace("object", "").ToLower());
                propertyInfo.SetValue(resultSingleItem, propGeneric.Invoke(null, new object[] { obj != null ? obj.Value : "", lang }));
            }
            else if (customMapperFieldData != null && customMapperFieldData.FieldType == CustomFieldType.SplitedText)
            {

            }
            else if (customMapperFieldData != null && customMapperFieldData.FieldType == CustomFieldType.Inherit)
            {

                var mapTypeMethod = typeof(MapperUtils).GetMethod("MapInheritArcheTypeClassToModel", BindingFlags.NonPublic | BindingFlags.Static);
                var propGeneric = mapTypeMethod.MakeGenericMethod(propertyInfo.PropertyType);
                propertyInfo.SetValue(resultSingleItem, propGeneric.Invoke(null, new object[] { archeRow, lang }));

            }
            else if (customMapperFieldData != null && customMapperFieldData.FieldType == CustomFieldType.Enum)
            {
                var mapTypeMethod = typeof(MapperUtils).GetMethod("MapEnumToModel", BindingFlags.NonPublic | BindingFlags.Static);
                var propGeneric = mapTypeMethod.MakeGenericMethod(ReflectionUtils.GetEnumType(propertyInfo.PropertyType.FullName));
                propertyInfo.SetValue(resultSingleItem, propGeneric.Invoke(null, new object[] { archeRow.Alias }));
            }
            else if (customMapperFieldData != null && customMapperFieldData.FieldType == CustomFieldType.RelatedObjectList)
            {
                var mapTypeMethod = typeof(MapperUtils).GetMethod("MapRelatedObjectListToModel", BindingFlags.NonPublic | BindingFlags.Static);
                var propGeneric = mapTypeMethod.MakeGenericMethod(propertyInfo.PropertyType);

                Type itemType = null;
                if (propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition()
                        == typeof(List<>))
                {
                    itemType = propertyInfo.PropertyType.GetGenericArguments()[0]; // use this...
                }
                propGeneric = mapTypeMethod.MakeGenericMethod(itemType);
                var obj = archeRow.Properties.SingleOrDefault(x => x.Alias.ToLower() == propertyInfo.Name.ToLower().Replace("object", "").ToLower());

                propertyInfo.SetValue(resultSingleItem, propGeneric.Invoke(null, new object[] { obj != null ? obj.Alias : "", obj != null ? obj.Value : "", lang }));
            }
            else if (propertyInfo.Name.ToLower() == "alias")
            {
                propertyInfo.SetValue(resultSingleItem, archeRow.Alias.ToString());
            }
        }

        private static T MapJsonToModel<T>(string json)
        {
            return JsonUtils.DeserializeJSON<T>(json);
        }

        private static IEnumerable<string> MapMultipleTextStringFieldToModel(string multilineString)
        {
            var splitCharsArray = "/\r/\n".ToCharArray();
            return multilineString.Split(splitCharsArray, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        private static IEnumerable<T> MapCsvFieldToModel<T>(string csv)
        {
            return csv.CsvToList<T>();
        }

        #endregion
    }
}