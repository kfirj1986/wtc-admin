﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WTC.Utils
{
    public class ParseUtils
    {
        public static T Convert<T>(string strValue)
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));
            if (converter != null)
            {
                try
                {
                    if (typeof(T) == typeof(bool))
                    {
                        if (strValue == "1")
                            return (T)converter.ConvertFromString("true");
                        else if (strValue == "0")
                            return (T)converter.ConvertFromString("false");
                    }
                    return (T)converter.ConvertFromString(strValue);
                }
                catch //extra handeling
                {
                    //parsing error
                    
                }
            }
            return default(T);
        }

        public static DateTime ConvertStringToDateTime(string date)
        {
            try
            {
                DateTime val;
                if (!string.IsNullOrEmpty(date))
                {
                    string[] formats;
                    formats = new string[]{"d/M/yyyy h:mm:ss tt", "d/M/yyyy h:mm tt",  
                   "dd/MM/yyyy hh:mm:ss", "d/M/yyyy h:mm:ss", 
                   "d/M/yyyy hh:mm tt", "d/M/yyyy hh tt", 
                   "d/M/yyyy h:mm", "d/M/yyyy h:mm", 
                   "dd/MM/yyyy hh:mm", "dd/M/yyyy hh:mm",
                    "d/MM/yyyy", "d/M/yyyy", 
                                   };
                    //DateTime dt = DateTime.ParseExact(date, System.Web.Configuration.WebConfigurationManager.AppSettings["DateFormat"].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    DateTime.TryParseExact(date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out val);

                    if (val == null || val == DateTime.MinValue)
                    {
                        formats = new string[]{"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",  
                   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
                   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
                   "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
                   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm",
                    "MM/d/yyyy", "M/d/yyyy", 
                                   };
                        DateTime.TryParseExact(date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out val);

                        if (val == null || val == DateTime.MinValue)
                        {
                            formats = new string[]{"yyyy/M/d h:mm:ss tt", "yyyy/M/d h:mm tt",  
                            "yyyy/MM/dd hh:mm:ss", "yyyy/M/d h:mm:ss", 
                            "yyyy/M/d hh:mm tt", "yyyy/M/d hh tt", 
                            "yyyy/M/d h:mm", "yyyy/M/d h:mm", 
                            "yyyy/MM/dd hh:mm", "yyyy/M/dd hh:mm",
                             "yyyy/MM/d", "yyyy/M/d", 
                                   };
                            DateTime.TryParseExact(date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out val);

                            if (val == null || val == DateTime.MinValue)
                            {
                                formats = new string[]{"yyyy/d/M h:mm:ss tt", "yyyy/d/M h:mm tt",  
                            "yyyy/dd/MM hh:mm:ss", "yyyy/d/M h:mm:ss", 
                            "yyyy/d/M hh:mm tt", "yyyy/d/M hh tt", 
                            "yyyy/d/M h:mm", "yyyy/d/M h:mm", 
                            "yyyy/dd/MM hh:mm", "yyyy/dd/M hh:mm",
                             "yyyy/d/MM", "yyyy/d/M", 
                                   };
                                DateTime.TryParseExact(date, formats, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out val);

                                if (val == null || val == DateTime.MinValue)
                                {

                                }
                                else
                                {
                                    return val;
                                }
                            }
                            else
                            {
                                return val;
                            }

                        }
                        else
                        {
                            return val;
                        }

                    }
                    else
                    {
                        return val;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return DateTime.MinValue;
        }
    }
}