﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WTC.Utils
{
    public class CollectionUtils
    {
        /// <summary>
        ///     Transform csv string to List of T items
        /// </summary>
        /// <typeparam name="T">result list type</typeparam>
        /// <param name="str">csv input</param>
        /// <returns></returns>
        public static List<T> CsvToList<T>(string str)
        {
            return str.CsvToList<T>();
        }

        /// <summary>
        ///     Get list with unique items that are not shared between them
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="firstList"></param>
        /// <param name="secondList"></param>
        /// <returns></returns>
        public static List<T> GetListDiffItems<T>(List<T> firstList, List<T> secondList)
        {
            //List<T> diffItems = new List<T>();
            //IEnumerable<T> diffLeft = firstList.Except(secondList);
            //IEnumerable<T> diffRight = secondList.Except(firstList);
            //diffItems.AddRange(diffLeft);
            //diffItems.AddRange(diffRight);
            //return diffItems.ToList();
            var diffItems = firstList.Except(secondList).ToList();
            return diffItems.ToList();
        }

        /// <summary>
        ///     Check if any item from the first list exists in the second list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="firstList"></param>
        /// <param name="secondList"></param>
        /// <returns>the result will by true in case same item exists at both of the lists</returns>
        public static bool IsListsContainSameItems<T>(List<T> firstList, List<T> secondList)
        {
            return GetListDiffItems(firstList, secondList).Count == 0;
        }

        //    /// <summary>
        //    /// Convert From List of Type X to Type T
        //    /// </summary>
        //    /// <typeparam name="T"></typeparam>
        //    /// <param name="list"></param>
        //    /// <returns></returns>
        public static List<T1> ConvertListType<T2, T1>(List<T2> list)
        {
            var result = new List<T1>();
            if (list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    result.Add((T1)Convert.ChangeType(item, typeof(T1)));
                }
            }

            return result;
        }
    }

    #region Extension Methods

    public static class CollectionUtilsExtensionsMethod
    {
        /// <summary>
        ///     convert csv string to generic list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static List<T> CsvToList<T>(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return new List<T>();
            }
            var result = new List<T>();
            var strArray = str.Split(',');
            foreach (var strItem in strArray)
            {
                result.Add((T)Convert.ChangeType(strItem, typeof(T)));
            }
            return result;
        }

        /// <summary>
        ///     Convert Enumerator collection to generic list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerator"></param>
        /// <returns></returns>
        public static List<T> EnumeratorToList<T>(this IEnumerable<T> enumerator)
        {
            var result = new List<T>();
            if (enumerator.Count() > 0)
            {
                foreach (var item in enumerator)
                {
                    result.Add((T)Convert.ChangeType(item, typeof(T)));
                }
            }
            return result;
        }

        /// <summary>
        ///     Convert Generic List to String CSV
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="endSuffix"></param>
        /// <returns></returns>
        public static string ConvertListToCSV<T>(this List<T> list, string endSuffix = ".")
        {
            var result = string.Empty;
            var sbResult = new StringBuilder();
            foreach (var item in list)
            {
                sbResult.AppendFormat("{0}, ", item);
            }
            if (sbResult.Length > 2)
            {
                result = string.Format("{0}{1}", sbResult.ToString().Substring(0, sbResult.ToString().Length - 2),
                    endSuffix);
            }
            return result;
        }

        /// <summary>
        ///     Shuffle list items order
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public static void Shuffle<T>(this IList<T> list)
        {
            var rng = new Random();
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = rng.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }

    #endregion
}