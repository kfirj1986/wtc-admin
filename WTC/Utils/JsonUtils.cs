﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace WTC.Utils
{
    public class JsonUtils
    {
        /// <returns></returns>
        public static T DeserializeJSON<T>(string jsonString)
        {
            var javascriptSerializer = new JavaScriptSerializer();
            return javascriptSerializer.Deserialize<T>(jsonString);
        }
        /// <summary>
        ///     Serialize Json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Serialize<T>(T obj)
        {
            var result = string.Empty;
            var javascriptSerializer = new JavaScriptSerializer();
            return javascriptSerializer.Serialize(obj);
        }
    }
}