﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Web;
using umbraco;

namespace WTC.Utils
{
    public class ContentUtils
    {
        public static string GetPropertyValueOrEmptyString(IPublishedContent content, string propertyName)
        {
            var result = string.Empty;
            if (content == null)
                return string.Empty;

            if (content.HasValue(propertyName))
                result = content.GetProperty(propertyName).Value.ToString();
            return result;
        }

        public static string GetNodeMainName(umbraco.NodeFactory.Node node, string propertyName)
        {
            var result = string.Empty;
            if (node == null)
                return string.Empty;

            result = node.Name;
            if (node.HasProperty(propertyName))
                result = node.GetProperty(propertyName).Value.ToString();
            return result;
        }

        /// <summary>
        ///     Get umbraco content url by content id
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public static string GetPageUrl(int contentId)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            return umbracoHelper.Url(contentId);
        }

        public static dynamic GetPage(int pageId)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            return umbracoHelper.Content(pageId);
        }

        public static dynamic GetMultiTreeNodePickerPublishedContent(string treeNodeValue)
        {
            var result = new List<dynamic>();
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            if (!string.IsNullOrEmpty(treeNodeValue))
                foreach (var treeNodeId in treeNodeValue.Split(','))
                {
                    var treeNodeIntId = 0;
                    if (int.TryParse(treeNodeId, out treeNodeIntId) && treeNodeIntId > 0)
                    {
                        var content = umbracoHelper.Content(treeNodeIntId);
                        if (content != null && content.Id > 0)
                            result.Add(content);
                    }
                }

            return result;
        }

        public static Attempt<PublishStatus> AddContent(string name, int parentId, string contentTypeAlias, Dictionary<string, string> extraPropertyTypes)
        {
            var content = ApplicationContext.Current.Services.ContentService.CreateContent(name, parentId, contentTypeAlias, 0);
            foreach (var extraPropertyType in extraPropertyTypes)
            {
                content.SetValue(extraPropertyType.Key, extraPropertyType.Value);
            }

            content.ParentId = parentId;
            return ApplicationContext.Current.Services.ContentService.SaveAndPublishWithStatus(content);
        }

    }

    public static class ContentUtilsExtensionMethods
    {
        /// <summary>
        ///     Retrieve KeyValue dictionary for IPublishedContent object
        /// </summary>
        /// <param name="content"></param>
        /// <param name="dataTypeId"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Dictionary<int, string> GetPreValuePropertyDictionary(this IPublishedContent content, int dataTypeId, string propertyName)
        {
            var result = new Dictionary<int, string>();
            if (content == null || content.Id <= 0 || propertyName == string.Empty || !content.HasValue(propertyName))
            {
                return result;
            }
            var dataTypeDictionary = DataTypeUtils.GetDataTypePreValues(dataTypeId);
            var propertyValue = content.GetProperty(propertyName).Value;
            if (propertyValue == null || propertyValue.ToString() == string.Empty)
            {
                return result;
            }
            var propertyTextList = propertyValue.ToString().Split(',').ToList();
            foreach (var propertyText in propertyTextList)
            {
                if (dataTypeDictionary.ContainsValue(propertyText))
                {
                    var keyValueToInsert = dataTypeDictionary.Single(x => x.Value.Equals(propertyText));
                    result.Add(keyValueToInsert.Key, keyValueToInsert.Value);
                }
            }
            return result;
        }

        /// <summary>
        ///     Retrieve Multiple Treenode property value
        /// </summary>
        /// <param name="content"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static dynamic GetTreeNodePropertyList(this IPublishedContent content, string propertyName)
        {
            if (!string.IsNullOrEmpty(propertyName) && content.HasProperty(propertyName))
            {
                //var propertyList = content.GetPropertyValue(propertyName).Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                //var helper = new UmbracoHelper(UmbracoContext.Current);
                //var propertyDynamicCollection = helper.Content(propertyList);
                //return propertyDynamicCollection;
            }
            return new List<int>();
        }

        public static T MapContentToModel<T>(this IPublishedContent content, string lang = "") where T : new()
        {
            return MapperUtils.Map<T>(content.Properties.ToDictionary(x => x.PropertyTypeAlias, x => x.DataValue.ToString()), lang);
            ;
        }
    }
}