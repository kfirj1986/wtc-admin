﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco;
using Umbraco.Core;

namespace WTC.Utils
{
    public class DataTypeUtils
    {
        /// <summary>
        ///     Get PreValues DataType Dictionary
        /// </summary>
        /// <param name="dataTypeId"></param>
        /// <returns></returns>
        public static Dictionary<int, string> GetDataTypePreValues(int dataTypeId)
        {
            var result = new Dictionary<int, string>();
            if (dataTypeId > 0)
            {
                var itr = library.GetPreValues(dataTypeId);
                itr.MoveNext(); //move to first
                var preValues = itr.Current.SelectChildren("preValue", "");
                while (preValues.MoveNext())
                {
                    var preValueText = preValues.Current.Value;
                    if (!string.IsNullOrEmpty(preValueText))
                    {
                        var itemTypeId = 0;
                        int.TryParse(preValues.Current.GetAttribute("id", ""), out itemTypeId);
                        if (itemTypeId > 0)
                        {
                            if (!result.ContainsKey(itemTypeId))
                            {
                                result.Add(itemTypeId, preValueText);
                            }
                        }
                    }
                }
            }

            return result;
        }


        public static Dictionary<int, string> GetDataTypePreValues(string dataTypeAlias)
        {
            var dataTypeService = ApplicationContext.Current.Services.DataTypeService;
            var allTypes = dataTypeService.GetAllDataTypeDefinitions();
            var dataTypeId = allTypes.First(x => dataTypeAlias.InvariantEquals(x.Name)).Id;
            return GetDataTypePreValues(dataTypeId);
        }
        /// <summary>
        ///     Get document property key values
        /// </summary>
        /// <param name="dataTypeId"></param>
        /// <param name="propertyText"></param>
        /// <returns></returns>
        public static Dictionary<int, string> GetPropertyPreValues(int dataTypeId, string propertyText)
        {
            var result = new Dictionary<int, string>();
            var preValues = GetDataTypePreValues(dataTypeId);

            if (preValues.Count > 0)
            {
                if (!string.IsNullOrEmpty(propertyText))
                {
                    var textValues = propertyText.Split(',');
                    foreach (var preValueText in textValues)
                    {
                        if (preValues.ContainsValue(preValueText))
                        {
                            var selectedValue = preValues.SingleOrDefault(x => x.Value == preValueText);
                            result.Add(selectedValue.Key, selectedValue.Value);
                        }
                    }
                }
            }
            return result;
        }

        public static List<string> ConvertPreValueKeysToPreValueText(int dataTypeId, List<int> preValueIds)
        {
            var result = new List<string>();
            if (dataTypeId > 0 && preValueIds.Count > 0)
            {
                var preValueDictionary = GetDataTypePreValues(dataTypeId);
                foreach (var preValue in preValueIds)
                {
                    if (preValueDictionary.ContainsKey(preValue))
                    {
                        result.Add(preValueDictionary.Where(x => x.Key == preValue).First().Value);
                    }
                }
            }
            return result;
        }

        public static string ConvertDataTypeKeyToValue(int dataTpeId, int preValueId)
        {
            return ConvertPreValueKeysToPreValueText(dataTpeId, new List<int> { preValueId }).SingleOrDefault();
        }

        public static List<T> ConvertCheckBoxListToEnumsList<T>(string checkedCsvValue)
        {
            var result = new List<T>();
            if (!string.IsNullOrEmpty(checkedCsvValue))
            {
                var checkedValues = checkedCsvValue.CsvToList<string>();
                foreach (var checkedValue in checkedValues)
                {
                    T enumInstance = default(T);
                    try
                    {
                        enumInstance = (T)Enum.Parse(typeof(T), checkedValue.ToLower().Replace(" ", ""), true);
                    }
                    catch
                    {

                    }
                    if (enumInstance != null)
                        result.Add(enumInstance);
                }

            }

            return result;
        }


        //public static List<T> BindArchetypeModel<List<T>>(string json)  
        //{
        //    var result = new List<T>();
        //    if (!string.IsNullOrEmpty(json))
        //    {
        //    var dataContainer = JsonUtils.DeserializeJSON<ArchetypeModel>(json);
        //        //if (dataContainer == null)
        //        //    return default(T);

        //        foreach (var dataItem in dataContainer)
        //        {
        //          //  if(dataItem.Properties.Any(x=>x.Alias==))
        //        }


        //    }
        //    return result;
        //}
    }

}