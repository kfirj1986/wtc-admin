﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace WTC.Utils
{
    public class VortoUtils
    {
        public static string GetFieldValue(string languageExtension, string jsonContent)
        {
            if (string.IsNullOrEmpty(jsonContent) || string.IsNullOrEmpty(languageExtension))
                return "";
            var allTranslations = JsonUtils.DeserializeJSON<VortoResult>(jsonContent);
            if (allTranslations == null)
                return "";
            if (!allTranslations.Values.ContainsKey(languageExtension.ToLower()))
                return "";


            return allTranslations.Values.Single(x => x.Key.ToLower() == languageExtension.ToLower()).Value;

        }


        public class VortoResult
        {
            public Dictionary<string, string> Values { get; set; }
            public Guid DtdGuid { get; set; }
        }

        public static string GetDefaultLang()
        {
            string lang = WebConfigurationManager.AppSettings["DefaultLanguage"].ToString();
            return lang;
        }
    }
}