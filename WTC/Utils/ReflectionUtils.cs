﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WTC.Utils
{
    public class ReflectionUtils
    {
        public static List<string> GetListObjectsPropertiesNames(Type type)
        {
            var listType = GetListType(type);
            return GetObjectPropertiesNames(listType);
        }

        public static List<string> GetObjectPropertiesNames(Type type)
        {
            var result = new List<string>();
            foreach (var prop in type.GetProperties())
            {
                result.Add(prop.Name);
            }
            return result;
        }

        /// <summary>
        ///     Retreive object properties info
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>list with all object properties info</returns>
        public static List<PropertyData> GetObjectPropertiesData(object obj)
        {
            var result = new List<PropertyData>();
            foreach (var property in obj.GetType().GetProperties())
            {
                result.Add(new PropertyData
                {
                    Name = property.Name,
                    Value = GetPropertyValue(property.Name, obj),
                    TypeName = property.PropertyType.Name
                });
            }
            return result;
        }

        public static List<PropertyInfo> GetPropertiesInfo<T>()
        {
            return typeof(T).GetProperties().ToList();
        }

        public static List<string> GetObjectPropertiesValues(object obj)
        {
            var result = new List<string>();
            foreach (var prop in obj.GetType().GetProperties())
            {
                result.Add(GetPropertyValue(prop.Name, obj));
            }
            return result;
        }

        public static Dictionary<string, string> GetObjectPropertiesKeysAndValues(object obj)
        {
            var result = new Dictionary<string, string>();
            foreach (var prop in obj.GetType().GetProperties())
            {
                if (!result.ContainsKey(prop.Name))
                {
                    result.Add(prop.Name, GetPropertyValue(prop.Name, obj));
                }
            }
            return result;
        }

        public static string GetPropertyValue(string propertyName, object objectType)
        {
            var result = string.Empty;

            //for (int i = 0; i < objectType.GetProperties().Length; i++)
            //{
            //    var currentProp = objectType.GetProperties()[i];
            //  if (  currentProp.Name==propertyName)
            //  {

            //      result = objectType.GetProperty(currentProp.Name).GetValue(objectType, null).ToString();
            //      break;
            //  }

            foreach (var info in objectType.GetType().GetProperties())
            {
                if (info.Name == propertyName)
                {
                    var objValue = info.GetValue(objectType, null);
                    if (objValue != null)
                        result = objValue.ToString();
                    break;
                }
            }

            return result;
        }

        public static Type GetListType(Type classType)
        {
            return classType.GetGenericArguments().First();
        }

        public static string GetDisplayName(Type currentTypeModel, string propertyName)
        {
            var result = propertyName;
            var currentProperty = currentTypeModel.GetProperty(propertyName).CustomAttributes;

            foreach (var item in currentProperty)
            {
                if (item.AttributeType.FullName == "System.ComponentModel.DataAnnotations.DisplayAttribute")
                {
                    result = item.NamedArguments.First().TypedValue.Value.ToString();
                    break;
                }
            }

            return result;
        }

        /// <summary>
        ///     Retrieve property type name
        /// </summary>
        /// <param name="modelType"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static string GetPropertyTypeName(Type modelType, string propertyName)
        {
            return modelType.GetProperty(propertyName).PropertyType.Name;
        }

        public static IList<CustomAttributeNamedArgument> GetPropertyDataAnnotations(Type modelType, string propertyName)
        {
            IList<CustomAttributeNamedArgument> result = null;
            var currentProperty = modelType.GetProperty(propertyName).CustomAttributes;

            foreach (var item in currentProperty)
            {
                if (item.AttributeType.FullName == "ES.Domain.Models.DataAnnotations.DataTableView")
                {
                    result = item.NamedArguments;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        ///     Get Property data annotation by value
        /// </summary>
        /// <param name="modelType"></param>
        /// <param name="propertyName"></param>
        /// <param name="dataAnnotationName"></param>
        /// <returns></returns>
        public static string GetPropertyDataAnnotationValue(Type modelType, string propertyName,
            string dataAnnotationName)
        {
            var result = string.Empty;
            var currentProperty = modelType.GetProperty(propertyName).CustomAttributes;

            return result;
        }

        /// Check if property has attribute of specific type
        public static bool IsPropertyHasAttribute(PropertyInfo propertyInfo, Type type)
        {
            if (propertyInfo.CustomAttributes.Any())
            {
                foreach (var customAttributeData in propertyInfo.CustomAttributes)
                {
                    if (customAttributeData.AttributeType == type)
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Retrieves custom attribute data applied to member
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        public static T GetCustomAttributeData<T>(PropertyInfo propertyInfo) where T : new()
        {
            return (T)Convert.ChangeType(Attribute.GetCustomAttribute(propertyInfo, typeof(T)), typeof(T));
        }

        /// <summary>
        /// Get enum type by Reflection
        /// </summary>
        /// <param name="enumName">The enum full name</param>
        /// <returns>The enum object</returns>
        public static Type GetEnumType(string enumName)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var type = assembly.GetType(enumName);
                if (type == null)
                    continue;
                if (type.IsEnum)
                    return type;
            }
            return null;
        }
    }

    public class PropertyData
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string TypeName { get; set; }
    }
}