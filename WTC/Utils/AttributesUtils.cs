﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTC.Utils
{
    /// <summary>
    ///     Define custom property mapping method
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CustomMapperField : Attribute
    {
        public string FieldAlias { get; set; }
        public CustomFieldType FieldType { get; set; }
    }

    public enum CustomFieldType
    {
        ArchetypeField = 1, 
        MultipleTextString = 2, 
        CsvField = 3, 
        JsonField = 4, 
        VostroTranslated = 5,
        Image = 6,
        ImageGallery = 7,
        Childs = 8,
        Enum = 9,
        HTML = 10,
        Inherit = 11,
        VostroTranslatedHTML = 12,
        SimpleString = 13,
        RelatedObject = 14,
        RelatedObjectList = 15,
        Date = 16,
        SplitedText = 17

        //CustomIndex = 11
    }

    /// <summary>
    ///     Ignore any mapper binding
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreMapper : Attribute
    {
    }
}