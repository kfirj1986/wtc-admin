﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace WTC.Utils
{
    public static class MediaUtils
    {
        public static dynamic GetMediaByID(this DynamicPublishedContent currentNode, int mediaId)
        {
            IPublishedContent mediaContent = null;
            var result = new MediaItem();

            if (mediaId > 0)
            {
                var helper = new UmbracoHelper(UmbracoContext.Current);
                mediaContent = helper.TypedMedia(mediaId);
            }

            if (mediaContent != null)
            {
                result.ID = mediaContent.Id;
                result.URL = mediaContent.Url;
                result.Name = mediaContent.Name;
            }
            return result;
        }

        public static dynamic GetMediaByStringID(this DynamicPublishedContent currentNode, string mediaIdStr)
        {
            if (!string.IsNullOrEmpty(mediaIdStr))
            {
                var mediaId = 0;
                int.TryParse(mediaIdStr, out mediaId);
                if (mediaId > 0)
                {
                    return GetMediaByID(currentNode, mediaId);
                }
            }
            return new MediaItem();
        }

        public static dynamic GetMedia(this DynamicPublishedContent currentNode, string propertyName)
        {
            IPublishedContent mediaContent = null;
            var result = new MediaItem();

            if (currentNode.HasValue(propertyName))
            {
                var mediaId = currentNode.GetPropertyValue<int>(propertyName);
                if (mediaId > 0)
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    mediaContent = helper.TypedMedia(mediaId);
                }
            }

            if (mediaContent != null)
            {
                result.ID = mediaContent.Id;
                if (mediaContent.DocumentTypeAlias != "Folder")
                {
                    result.URL = mediaContent.Url;
                }
                result.Name = mediaContent.Name;
            }
            return result;
        }

        public static MediaItem GetMedia(this IPublishedContent currentNode, string propertyName)
        {
            var result = new MediaItem();

            if (currentNode.HasValue(propertyName))
            {
                var mediaId = currentNode.GetPropertyValue<int>(propertyName);
                if (mediaId > 0)
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    currentNode = helper.TypedMedia(mediaId);
                }
            }

            if (currentNode != null)
            {
                result.ID = currentNode.Id;
                if (currentNode.DocumentTypeAlias != "Folder")
                {
                    result.URL = currentNode.Url;

                }
                result.Name = currentNode.Name;
            }
            return result;
        }

        public static string GetMediaUrl(this DynamicPublishedContent currentNode, string propertyName)
        {
            var result = GetMedia(currentNode, propertyName);
            return result.URL;
        }

        public static string GetMediaName(this DynamicPublishedContent currentNode, string propertyName)
        {
            var result = GetMedia(currentNode, propertyName);
            return result.Name;
        }

        public static string GetMediaID(this DynamicPublishedContent currentNode, string propertyName)
        {
            var result = GetMedia(currentNode, propertyName);
            return result.ID;
        }

        public static IMedia CreateMedia(string mediaItemName, string mediaTypeAlias,
            int mediaParentId, HttpPostedFile file)
        {
            var mediaService = ApplicationContext.Current.Services.MediaService;
            var newMedia = mediaService.CreateMedia(mediaItemName, mediaParentId, mediaTypeAlias);
            if (file != null)
            {
                newMedia.SetValue(Constants.Conventions.Media.File, file);
            }
            mediaService.Save(newMedia);

            return newMedia;
        }
        public static IMedia CreateMediaFromFileStream(string mediaItemName, string mediaTypeAlias,
          int mediaParentId, FileStream file)
        {
            var mediaService = ApplicationContext.Current.Services.MediaService;
            var newMedia = mediaService.CreateMedia(mediaItemName, mediaParentId, mediaTypeAlias);
            if (file != null)
            {
                newMedia.SetValue(Constants.Conventions.Media.File, mediaItemName, file);
            }
            mediaService.Save(newMedia);

            return newMedia;
        }

        /// <summary>
        ///     Return IMedia List of media item childrens
        /// </summary>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public static List<IMedia> GetMediaItemChildrens(int mediaId)
        {
            var result = new List<IMedia>();
            var mediaService = ApplicationContext.Current.Services.MediaService;
            return mediaService.GetDescendants(mediaId).ToList();
        }

        /// <summary>
        ///     Retrieve IMedia by mediaId
        /// </summary>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public static IMedia GetMedia(int mediaId)
        {
            var mediaService = ApplicationContext.Current.Services.MediaService;
            return mediaService.GetById(mediaId);
        }

        /// <summary>
        ///     Retrieve media published content
        /// </summary>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public static IPublishedContent GetMediaPublishedContent(int mediaId)
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var mediaPicker = helper.TypedMedia(mediaId);
            return mediaPicker;
        }

        /// <summary>
        ///     Retrieve media url
        /// </summary>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public static string GetMediaUrl(int mediaId)
        {
            var mediaPublshedContent = GetMediaPublishedContent(mediaId);
            if (mediaPublshedContent != null)
            {
                return mediaPublshedContent.Url;
            }
            return string.Empty;
        }

        /// <summary>
        ///     Permanantly delete IMedia object
        /// </summary>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public static bool DeleteMedia(int mediaId)
        {
            var result = false;
            var mediaService = ApplicationContext.Current.Services.MediaService;
            var mediaToDelete = GetMedia(mediaId);
            if (mediaToDelete != null)
            {
                mediaService.Delete(mediaToDelete);
                result = true;
            }
            return result;
        }

        /// <summary>
        ///     Permanantly delete IMedia Object by Key(Guid)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool DeleteMedia(Guid key)
        {
            var result = false;
            var mediaService = ApplicationContext.Current.Services.MediaService;
            var mediaToDelete = mediaService.GetById(key);
            if (mediaToDelete != null)
            {
                mediaService.Delete(mediaToDelete);
                result = true;
            }
            return result;
        }
    }

    public class MediaItem
    {
        public int ID { get; set; }
        public string URL { get; set; }
        public string Name { get; set; }

        public bool HasValue
        {
            get { return ID > 0; }
        }
    }

    public class MediaResult
    {
        public string src { get; set; }
        public Dictionary<string, string> FocalPoint { get; set; }
    }
}