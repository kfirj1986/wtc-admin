﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class Package : Master
    {
        public string packageCode { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.HTML)]
        public string remarks { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.HTML)]
        public string conditions { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.HTML)]
        public string general { get; set; }
        public string stamps { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }

    }
}