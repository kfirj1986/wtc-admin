﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class FlyAndDriveCar
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public virtual string mainName { get; set; }
        public string groupCode { get; set; }
        public string gWID { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public FlyAndDriveCarTransmission transmissionObject { get; set; }
        public string transmission { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public FlyAndDriveCarCategory categoryObject { get; set; }
        public string category { get; set; }


        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public FlyAndDriveCarClass carClassObject { get; set; }
        public string carClass { get; set; }

        public bool airCondition { get; set; }
        public string doorsNumber { get; set; }
        public string adultsNumber { get; set; }
        public string childrenNumber { get; set; }
        public string bigSuitcaseNumber { get; set; }
        public string smallSuitcaseNumber { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string surcharges { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image carGroupImage { get; set; }

        #region Insurance
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string insurance { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> insuranceIncludes { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> insuranceUnincludes { get; set; }

        #endregion

    }

    public class FlyAndDriveCarClass : Master
    {

    }

    public class FlyAndDriveCarCategory : Master
    {
    }

    public class FlyAndDriveCarTransmission
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public virtual string mainName { get; set; }
    }

}