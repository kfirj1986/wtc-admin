﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTC.Models
{
    public class Currency
    {
        public string originalName { get; set; }
        public string hebrewName { get; set; }
        public string code { get; set; }
        public string symbol { get; set; }
        public string exchangeRate { get; set; }
        public bool isPopular { get; set; }
        public int order { get; set; }
        
        
    }
}