﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class Master
    {
        public string id { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public virtual string mainName { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string mainDescription { get; set; }
    }
}