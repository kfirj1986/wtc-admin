﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class Seo
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string description { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string keyWords { get; set; }
        public string friendlyURL { get; set; }
        public string robots { get; set; }
        
    }
}