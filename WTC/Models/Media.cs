﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class Media
    {
        //[CustomMapperField(FieldType = CustomFieldType.Image)]
        //public string mainImage { get; set; }


        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image mainImage { get; set; }

        public string linkToEmbeddedVideo { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ImageGallery)]
        public List<Image> imageGallery { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ImageGallery)]
        public List<Image> secondaryGallery { get; set; }

    }

    public class Image
    {
        public string src { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string alt { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }
    }
}