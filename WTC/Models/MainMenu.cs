﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class MainMenu
    {
        //[CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        //public IEnumerable<FooterColumnWebModel> FooterColumns { get; set; }
        //[CustomMapperField(FieldType = CustomFieldType.JsonField)]
        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public IEnumerable<MenuItem> MenuItem { get; set; }

    }

    public class Footer
    {

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<FooterSection> sections { get; set; }

        #region Link Gallery
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<GalleryBunners> footFooter { get; set; }
        #endregion

    }

    public class MenuItem
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string tabName { get; set; }
        public string link { get; set; }
        public string order { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<SubMenuColumn> subMenuHeadline { get; set; }

    }

    public class SubMenuColumn
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string text { get; set; }
        public string link { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<SubMenuItem> subMenuItem { get; set; }
    }

    public class FooterSection
    {
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<SubMenuItem> sectionContent { get; set; }
    }

    public class SubMenuItem
    {
        public string alias { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }

        
        #region linkSubMenu
        public string titleHref { get; set; }
        public string text { get; set; }

                [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<SubMenuColumn> linkSubMenu { get; set; }
        #endregion

        #region imageSubMenu
        public string image { get; set; }
        public string titleOnImage { get; set; }
        public string textOnImage { get; set; }
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string href { get; set; }
        #endregion

        #region textEditorSubMenu
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string richTextEditor { get; set; }
        
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string textArea { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string textAreaSubMenu { get; set; }
        #endregion

    }

}