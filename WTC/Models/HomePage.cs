﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class HomePage
    {
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<GalleryInGalleryBunners> mainGallery { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<ComponentsGallery> contentComponents { get; set; }

        public string text { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }

    }

    public class GalleryInGalleryBunners
    {
        #region Outside Gallery
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<BannersVideoAndImageGallery> outsideGallery { get; set; }

        #endregion


        #region Inside Gallery
        public List<string> image { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<BannersVideoAndImageGallery> insideGallery { get; set; }
        #endregion
    }


    public class BannersVideoAndImageGallery
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string text { get; set; }
        public string linkToEmbeddedVideo { get; set; }
        public string linkURL { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image image { get; set; }
    }


}