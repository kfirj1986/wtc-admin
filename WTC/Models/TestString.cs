﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class TestString
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string text { get; set; }
    }
}