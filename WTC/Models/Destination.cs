﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{

    public class Location
    {
        public string longitude { get; set; }
        public string latitude { get; set; }
        public bool displayOnDestinationMap { get; set; }
        public string linkToPage { get; set; }
    }

    public class ExtandedLocation
    {
        public string address { get; set; }
        public string zipCode { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string locationDescription { get; set; }

    }

    public class DesinationInfo
    {
        public string population { get; set; }
        public string squareKilometer { get; set; }
        public string currencies { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObjectList)]
        public List<Currency> currenciesObject { get; set; }
        public string languages { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObjectList)]
        public List<Language> languagesObject { get; set; }
        public string gMT { get; set; }
        public string countryDialingCode { get; set; }
        public string voltage { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<Tab> extendedInfo { get; set; }
    }

    public class Destination : Master
    {

        /// <summary>
        /// Index Field
        /// </summary>
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string areaName { get; set; }
        public string area { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Area areaObject { get; set; }

        public string iATACode { get; set; }

        public virtual string airportName { get; set; }

        public string gWID { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public List<Destination> country { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public List<Destination> city { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public List<Airport> airport { get; set; }

        //[CustomMapperField(FieldType = CustomFieldType.Enum)]
        //public DestinationTypes Type { get; set; }
        public string alias { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Location Location { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public ExtandedLocation ExtandedLocation { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public DesinationInfo DesinationInfo { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }

        public string lobbyPageFriendlyURL { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string state { get; set; }


        public string flightsCode { get; set; }
        public string hotelsCode { get; set; }
        public string chartersCode { get; set; }

        //public string carGWId { get; set; }
    }

    public class Continent : Destination
    {

    }

    public class Country : Destination
    {

    }

    public class City : Destination
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string test { get; set; }
    }

    public class Area : Destination
    {
    }


    public class Airport : Destination
    {
        //[CustomMapperField(FieldType = CustomFieldType.SimpleString)]
        //public override string mainName { get; set; }


    }


    public class Airline : Master
    {
        public string iATACode { get; set; }
    }

    public enum DestinationTypes
    {
        area = 1,
        continent = 2,
        country = 3,
        city = 4,
        airport = 5
    }
}