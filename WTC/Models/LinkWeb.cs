﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTC.Models
{
    public class LinkWeb
    {
        public string Caption { get; set; }
        public string Link { get; set; }
        public string NewWindow { get; set; }
        public string Title { get; set; }
    }
}