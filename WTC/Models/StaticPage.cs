﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class StaticPage : Master
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string pageContent { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }
    }

    public class AboutUs
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string pageContent { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string pageTitle { get; set; }


        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<GalleryBunners> bannersGallery { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }
    }

    public class ContactUs
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string pageContent { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string pageTitle { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<GalleryBunners> mainGallery { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }
    }

    public class SiteInfo
    {
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> siteContactUsEmail { get; set; }
        public string siteSupportPhone { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string siteAddress { get; set; }
        public string openingHours { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string cRSDisplayName { get; set; }
        public string cSREmail { get; set; }
        public string siteContactUsPhone { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string agencyNameDisplay { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> contactAgentOrder { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> orderRecipients { get; set; }
        public string weekEndTime { get; set; }
        public string googleAnalyticsID { get; set; }
        public string additionalPhoneFee { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image logo { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image noImage { get; set; }


        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Location Location { get; set; }

        #region Site Configuration
        public string umbracoUrl { get; set; }
        public string appUrl { get; set; }
        public string appAPIKey { get; set; }

        #endregion

    }
}