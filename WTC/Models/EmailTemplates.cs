﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class EmailTemplates
    {
    }

    public class ConfirmationEmailTemplate
    {
        #region General Fields
        public string id { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public virtual string mainLayout { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string paymentSection { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string customerSection { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string agentSection { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string passenger { get; set; } 
        #endregion

        #region Segments Fields

        #region Hotel Package Segment
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelPackageSegmentLayout { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelPackageHotelAndFlight { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelPackageRoom { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelPackageRoomPassenger { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelPackageRemarks { get; set; } 
        #endregion
        
        #region Hotel Segment
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelSegmentLayout { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelHotel { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelHotelRoom { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string hotelHotelRoomPassenger { get; set; }
        #endregion

        #region Flight Segment
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string flightSegmentLayout { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string flightLeg { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string flightSegment { get; set; }
        #endregion

        #region Fly And Drive Segment
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string flyAndDriveSegmentLayout { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string flyAndDriveFlightAndCar { get; set; }
        #endregion
        
        #endregion
    }
    
}