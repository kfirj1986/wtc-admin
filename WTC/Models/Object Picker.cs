﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{


    public class Object_Picker
    {
        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public List<Fields> fieldsets { get; set; }
        public string hotel { get; set; }
        public string package { get; set; }
        public string organizedTrip { get; set; }
        public string takePricesAndDatesFromTrip { get; set; }
        public string link { get; set; }
    }

    public class Fields
    {
        public List<GeneralProperties> properties { get; set; }
        public string alias { get; set; }
        public bool disabled { get; set; }
        public string id { get; set; }
        public object releaseDate { get; set; }
        public object expireDate { get; set; }
        public string allowedMemberGroups { get; set; }
    }

    public class GeneralProperties
    {
        public string alias { get; set; }
        public string value { get; set; }
    }


    public enum ESegment
    {
        package = 1,
        hotel = 2,
        trip = 3,
    }

}