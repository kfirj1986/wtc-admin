﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class OrganizedTrip : Master
    {
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> includes { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> unIncludes { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string terms { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> remarks { get; set; }
        public string nights { get; set; }
        public string boardBase { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public BoardBase boardBaseObject { get; set; }
        public string mapLinkEmbedded { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> whyUs { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public IEnumerable<Dates> tripDates { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public IEnumerable<Itineraries> itineraries { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }

        public string stamps { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObjectList)]
        public List<Stamp> stampsObject { get; set; }
    }

    public class Dates 
    {
        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public IEnumerable<TripDate> tripDate { get; set; }
    }

    public class Itineraries
    {
        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public IEnumerable<Itinerary> itinerary { get; set; }
    }

    public class TripDate : Master
    {
        [CustomMapperField(FieldType = CustomFieldType.Date)]
        public string start { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Date)]
        public string end { get; set; }
        public string pricePerPerson { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string priceBasis { get; set; }
        public string currency { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<AlternativePrices> alternativePrices { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Currency currencyObject { get; set; }
        public string guide { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Guide guideObject { get; set; }
        /// <summary>
        /// Index Field
        /// </summary>
        //[CustomMapperField(FieldType = CustomFieldType.CustomIndex)]
        public string guideName { get; set; }

        //[CustomMapperField(FieldType = CustomFieldType.Date)]
        public string outGoingDepartureTime { get; set; }

        //[CustomMapperField(FieldType = CustomFieldType.Date)]
        public string outGoingArrivalTime { get; set; }
        public string outGoingDepartureAirport { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Airport outGoingDepartureAirportObject { get; set; }

        public string outGoingDepartureAirline { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Airline outGoingDepartureAirlineObject { get; set; }
        public string outGoingArrivalAirport { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Airport outGoingArrivalAirportObject { get; set; }
        public string outGoingFlightNumber { get; set; }
        //[CustomMapperField(FieldType = CustomFieldType.Date)]
        public string returnDepartureTime { get; set; }
        //[CustomMapperField(FieldType = CustomFieldType.Date)]
        public string returnArrivalTime { get; set; }
        public string returnDepartureAirport { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Airport returnDepartureAirportObject { get; set; }
        public string returnArrivalAirport { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Airport returnArrivalAirportObject { get; set; }
        public string returnAirline { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Airline returnAirlineObject { get; set; }
        public string returnFlightNumber { get; set; }
        public string itinerary { get; set; }
        //[CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        //public Itinerary itineraryObject { get; set; }
        
    }

    public class Itinerary
    {
        public string id { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string description { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string subTitle { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string generalRemarks { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Childs)]
        public IEnumerable<ItineraryDay> itineraryDay { get; set; }

        public string mapLink { get; set; }

    }

    public class ItineraryDay
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string description { get; set; }

        public string hotel { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Hotel hotelObject { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }
    }


    public class AlternativePrices
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string priceBasis { get; set; }
        public string price { get; set; }
    }
}