﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class LobbyPage : Master
    {
        public string id { get; set; }
        public string alias { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<GalleryBunners> mainGallery { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<ComponentsGallery> contentComponents { get; set; }

        public string text { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }

        public string segment { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Segment segmentObject { get; set; }

        public string subject { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Segment subjectObject { get; set; }

        #region Specific Lobby Page Adds
        public string country { get; set; }

        public string destination { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Destination destinationObject { get; set; }
        #endregion

        #region General Lobby Page
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslatedHTML)]
        public string htmlContent { get; set; }
        #endregion
    }

    public class ComponentsGallery
    {
        //[CustomMapperField(FieldType = CustomFieldType.Enum)]

        //public ComponentsTypes componentsType { get; set; }
        public string alias { get; set; }


        #region General

        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string subTitle { get; set; }
        public string galleryType { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public EGallery galleryTypeObject { get; set; }
        #endregion

        #region Query Promo Gallery
        public string destination { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Destination destinationObject { get; set; }

        public string segment { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Segment segmentObject { get; set; }

        public string subject { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Subject subjectObject { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Date)]
        public string fromDateRangeCheckin { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Date)]
        public string toDateRangeCheckin { get; set; }
        
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string linkTitle { get; set; }
        public string linkURL { get; set; }

        #endregion

        #region Free Promo Gallery
        //public IEnumerable<Promotion> Promotions { get; set; }
        //public string Promotions { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<FreePromotion> promotions { get; set; }

        #endregion

        #region Aggregate Promo Gallery
        [CustomMapperField(FieldType = CustomFieldType.SplitedText)]
        public string aggregatePromoCode { get; set; } 
        #endregion

        #region Bunner Gallery
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<BunnersImage> bunnerGallery { get; set; }

        #endregion

        #region Link Gallery
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<GalleryBunners> linkGallery { get; set; }
        #endregion

        #region Textual Bunner Gallery
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<GalleryBunners> textualBunner { get; set; }
        #endregion

        #region Text Editor
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string textEditor { get; set; }
        #endregion

        #region Tab List Component
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<Tab> tabList { get; set; }
        #endregion

        #region Customers Stories Component
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<CustomerStory> customerStories { get; set; }
        
        #endregion

        #region Text Section Component
        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image image { get; set; }
        #endregion
    }

    public class GalleryBunners
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }

        #region Video
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string text { get; set; }
        public string linkToEmbeddedVideo { get; set; }
        public string linkURL { get; set; }
        public string link { get; set; }


        #endregion


        #region Images/Bunners
        public List<string> image { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<BunnersImage> imageGallery { get; set; }

        public string galleryType { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public EGallery galleryTypeObject { get; set; }

        #endregion

    }


    public class CustomerStory
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string text { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image image { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string name { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string customerRole { get; set; }
    }

    public class Tab
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string text { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image image { get; set; }

    }

    public class BunnersImage
    {
        //public List<string> image { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Image)]
        public Image image { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string title { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string text { get; set; }
        public string linkURL { get; set; }

        public string linkToEmbeddedVideo { get; set; }
    }

    public enum ELobbyPageTemplates
    {
        destinationPage = 1,
        segmentInDestinationPage = 2,
        segmentPage = 3,
        subjectPage = 4
    }


    public enum ComponentsTypes
    {
        queryPromoGallery = 1,
        freePromoGallery = 2,
        bunnerGallery = 3,
        linkGallery = 4,
        textualBunnerGallery = 5,
        textEditor = 6,
        generalInfoComponent = 7
    }

    public class EGallery : Master
    {
        public string code { get; set; }
        public string textualCode { get; set; }
    }




}