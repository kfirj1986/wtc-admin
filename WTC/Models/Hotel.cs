﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class Hotel : Master
    {
        public Hotel()
        {
            rooms = new List<Models.HotelRoom>();
        }



        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }
        public virtual string hotelName { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.SimpleString)]
        public override string mainName { get; set; }
        public string CityName { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public City cityObject { get; set; }
        public string city { get; set; }
        public string rank { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string shortDescription { get; set; }
        public string siteLink { get; set; }
        public string gWID { get; set; }
        public string tripAdvisorID { get; set; }
        public string numberOfRooms { get; set; }
        public string checkInTime { get; set; }
        public string checkOutTime { get; set; }
        public string hotelType { get; set; }
        public string hotelTypeName { get; set; }
        public string stamps { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<TestString> highlights { get; set; }
        public string facilities { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObjectList)]
        public List<Facility> facilitiesObject { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<HotelRoom> rooms { get; set; }

        #region Media
        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }
        #endregion

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public ExtandedLocation ExtandedLocation { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Location Location { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Contact Contact { get; set; }

        public bool recommended { get; set; }

        public string hotelChain { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public HotelChain hotelChainObject { get; set; }
        

    }


    public class HotelRoom : Master
    {
        public string size { get; set; }
        public string gWID { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }
        public string facilities { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObjectList)]
        public List<Facility> facilitiesObject { get; set; }
    }

    public class Facility : Master
    {
        public string code { get; set; }
    }

    public class HotelType : Master
    {
    }

    public class HotelChain : Master
    {
    }
}