﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;
using Zone.ArchetypeMapper;


namespace WTC.Models
{

    public class Guide : Master
    {
        public string phone { get; set; }
        public string mail { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public IEnumerable<Profession> profession { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }
    }


    public class Profession
    {
        public string Destination { get; set; }
        public string Years { get; set; }
    }
}