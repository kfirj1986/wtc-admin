﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTC.Models
{
    public class Contact
    {
        public string phone { get; set; }
        public string fax { get; set; }
        public string eMail { get; set; }
    }
}