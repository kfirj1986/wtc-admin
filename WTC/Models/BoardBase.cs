﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTC.Models
{
    public class BoardBase 
    {
        public string originalName { get; set; }
        public string hebrewName { get; set; }
        public string code { get; set; }
    }
}