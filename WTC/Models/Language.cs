﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class Language
    {
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string mainName { get; set; }
        public string originalName { get; set; }
        public string hebrewName { get; set; }
        public string code { get; set; }
    }
}