﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class Subject : Master
    {
        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Seo Seo { get; set; }
        public string odiseaCode { get; set; }
        
    }
}