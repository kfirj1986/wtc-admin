﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Utils;

namespace WTC.Models
{
    public class Promotion : Master
    {
        [CustomMapperField(FieldType = CustomFieldType.Date)]
        public string fromDate { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.Date)]
        public string toDate { get; set; }
        public string nights
        {
            get
            {
                if (fromDate != null && toDate != null)
                {
                    try
                    {

                        DateTime from = ParseUtils.ConvertStringToDateTime(fromDate);
                        DateTime to = ParseUtils.ConvertStringToDateTime(toDate);
                        if (from != null && from > DateTime.MinValue && to != null && to > DateTime.MinValue && to >= from)
                        {
                            return (to - from).TotalDays.ToString();
                        }
                    }
                    catch (Exception ex)
                    {

                        return ex.Message + ex.InnerException;
                    }
                    return "Error In Date Time"; 
                }
                else
                {
                    return null;
                }
            }
        }
        public string priceBefore { get; set; }
        public string actualPrice { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string priceBasis { get; set; }
        public string currency { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Currency currencyObject { get; set; }
        public string boardBaseCode { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public BoardBase boardBaseCodeObject { get; set; }
        public string promotionBasicType { get; set; }
        public string tags { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.VostroTranslated)]
        public string remarks { get; set; }

        [CustomMapperField(FieldType = CustomFieldType.Inherit)]
        public Media Media { get; set; }

        #region Destination
        public string destinations { get; set; }

        #endregion

        #region Tag
        public string subjects { get; set; }
        #endregion

        #region Segment
        [CustomMapperField(FieldType = CustomFieldType.ArchetypeField)]
        public List<Object_Picker> objectPicker { get; set; }
        #endregion        

    }

    public class FreePromotion
    {
        public string promoPicker { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.RelatedObject)]
        public Promotion promoPickerObject { get; set; }
        //public string promoCode { get; set; }
        [CustomMapperField(FieldType = CustomFieldType.SplitedText)]
        public string livepromoCode { get; set; }

    }


}