﻿using Examine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using UmbracoExamine;
using WTC.IRepository;
using WTC.Models;
using WTC.Repository;
using System.Web.Http.Cors;

namespace WTC.Controllers
{
    //[EnableCors(origins: "127.0.0.1", headers: "*", methods: "*")]
    [EnableCors(origins: "http://localhost:29283", headers: "*", methods: "*")]
    [RoutePrefix("Dudu")]
    public class MainController : UmbracoApiController
    {
        public IHotelsRepository HotelsRepository { get { return new HotelsRepository(); } }
        public IFlyAndDriveCarsRepository CarsRepository { get { return new FlyAndDriveCarsRepository(); } }
        public IDestinationsRepository DestinationsRepository { get { return new DestinationsRepository(); } }
        public IOrganizedTripsRepository OrganizedTripsRepository { get { return new OrganizedTripsRepository(); } }
        public ILayoutRepository LayoutRepository { get { return new LayoutRepository(); } }
        public ILobbyPageRepository LobbyPageRepository { get { return new LobbyPageRepository(); } }
        public IPromotionsRepository PromotionsRepository { get { return new PromotionsRepository(); } }
        public ISubjectRepository SubjectRepository { get { return new SubjectRepository(); } }
        public IStaticPagesRepository StaticPagesRepository { get { return new StaticPagesRepository(); } }
        public IPackagesRepository PackagesRepository { get { return new PackagesRepository(); } }
        public ISettingsRepository SettingsRepository { get { return new SettingsRepository(); } }
        public IMailTemplatesRepository MailTemplatesRepository { get { return new MailTemplatesRepository(); } }



        [HttpGet]
        [ActionName("GetTestPage")]
        public System.Web.Http.Results.JsonResult<GridTest> GetTestPage()
        {
            string lang = getLangFromHeader();

            GridTest oGridTest = new GridTest();
            var searchProvider = ExamineManager.Instance.SearchProviderCollection["Searcher"];
            var searchCriteria = searchProvider.CreateSearchCriteria(IndexTypes.Content);

            Examine.SearchCriteria.IBooleanOperation query = searchCriteria.Field("nodeTypeAlias", "tEST");

            var searchResults = searchProvider.Search(query.Compile());
            if (searchResults != null)
            {
                foreach (var item in searchResults)
                {
                    oGridTest = Utils.ExamineUtils.MapSearcResultItemToModel<GridTest>(item, lang);

                    if (oGridTest != null)
                    {
                        return Json(oGridTest);
                    }
                }

                //IEnumerable<linkSubMenu> dfudu = Utils.ExamineUtils.MapSearchResultsToModel<linkSubMenu>(searchResults).ToList();



                //foreach (var searchResult in searchResults)
                //{
                //    Models.Guide oGuide = new Models.Guide();

                //    if (searchResult.Fields.ContainsKey("mainName"))
                //    {
                //        oGuide.mainName = searchResult.Fields["mainName"].ToString();
                //    }
                //}
                //return Json(searchResults);
            }

            return null;
        }
       

        #region Home Page
        [HttpGet]
        [ActionName("GetHomePage")]
        public HomePage GetHomePage()
        {
            string lang = getLangFromHeader();

            return StaticPagesRepository.GetHomePage(lang).FirstOrDefault();
        }
        #endregion

        #region Destinations
        [HttpGet]
        [Route("GetAllDest")]
        public List<Destination> GetAllDestinations()
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetAllDestinations(lang);
        }

        [HttpGet]
        [ActionName("GetDestinationByID")]
        public List<Destination> GetDestinationByID(int id)
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetDestinationByID(lang, id);
        }



        [HttpGet]
        [ActionName("GetDestinationTree")]
        public List<Destination> GetDestinationTree()
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetDestinationTree(lang);
        }


        [HttpGet]
        [ActionName("GetDestinationTreeByID")]
        public Destination GetDestinationTreeByID(int id)
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetDestinationTreeByID(lang, id);
        }

        [HttpGet]
        [ActionName("GetContinents")]
        public List<Continent> GetContinents(string id)
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetContinents(lang, id);
        }

        [HttpGet]
        [ActionName("GetCountries")]
        public List<Country> GetCountries(string id)
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetCountries(lang, id);
        }

        [HttpGet]
        [ActionName("GetCities")]
        public List<City> GetCities(string id)
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetCities(lang, id);
        }

        [HttpGet]
        [ActionName("GetAirPorts")]
        public List<Airport> GetAirPorts(string id)
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetAirPorts(lang, id);
        }

        [HttpGet]
        [ActionName("GetAreas")]
        public List<Area> GetAreas(string id)
        {
            string lang = getLangFromHeader();

            return DestinationsRepository.GetAreas(lang, id);
        }
        #endregion

        #region Vorto Languages
                [HttpGet]
        [ActionName("GetAllVortoLanguages")]
        public List<Language> GetAllVortoLanguages()
        {
            List<Language> LanguagesList = new List<Language>();
            var localService = ApplicationContext.Services.LocalizationService;

            foreach (var lang in localService.GetAllLanguages())
            {
                LanguagesList.Add(new Language() {
                    code = lang.IsoCode,
                    originalName = lang.CultureInfo.NativeName,
                    //hebrewName = lang.CultureInfo.DisplayName
                });           
            }

            return LanguagesList;
        }
        #endregion

        #region Segments
        #region Organized Trips
        [HttpGet]
        [ActionName("GetOrganizedTrips")]
        public List<OrganizedTrip> GetOrganizedTrips(string id)
        {
            string lang = getLangFromHeader();

            return OrganizedTripsRepository.GetOrganizedTrips(lang, id);
        }

        [HttpGet]
        [ActionName("GetAllTripsByDestinationID")]
        public List<OrganizedTrip> GetAllTripsByDestinationID(string id)
        {
            string lang = getLangFromHeader();

            return OrganizedTripsRepository.GetAllTripsByDestinationID(lang, id);
        }

        [HttpGet]
        [ActionName("GetOrganizedTripByID")]
        public OrganizedTrip GetOrganizedTripByID(int id)
        {
            string lang = getLangFromHeader();

            return OrganizedTripsRepository.GetOrganizedTripsByID(lang, id);
        }

        [HttpGet]
        [ActionName("GetOrganizedTripsTree")]
        public List<OrganizedTrip> GetOrganizedTripsTree()
        {
            string lang = getLangFromHeader();

            return OrganizedTripsRepository.GetOrganizedTripsTree(lang);
        }

        [HttpGet]
        [ActionName("GetGuides")]
        public List<Guide> GetGuides(string id)
        {
            string lang = getLangFromHeader();

            return OrganizedTripsRepository.GetGuides(lang, id);
        }
        #endregion

        #region Hotels
        [HttpGet]
        [ActionName("GetHotels")]
        public List<Hotel> GetHotels(string id, string areaId = "", string countryId = "", string cityId = "")
        {
            string lang = getLangFromHeader();

            return HotelsRepository.GetHotels(lang, id, areaId, countryId, cityId);
        }

        [HttpGet]
        [ActionName("GetHotelByCityIDAndName")]
        public List<Hotel> GetHotelByCityIDAndName(string cityId, string name)
        {
            string lang = getLangFromHeader();

            return HotelsRepository.GetHotelByCityIDAndName(lang, cityId, name);
        }

        [HttpGet]
        [ActionName("GetHotelTreeByID")]
        public Hotel GetHotelTreeByID(string id)
        {
            string lang = getLangFromHeader();

            return HotelsRepository.GetHotelTreeByID(lang, id);
        }


        #endregion

        #region Packages
        [HttpGet]
        [ActionName("GetPackages")]
        public List<Package> GetPackages(string id)
        {
            string lang = getLangFromHeader();

            return PackagesRepository.GetPackages(lang, id);
        }

        [HttpGet]
        [ActionName("GetPackageByOdiseaCode")]
        public List<Package> GetPackageByOdiseaCode(string OdiseaCode)
        {
            string lang = getLangFromHeader();

            return PackagesRepository.GetPackageByOdiseaCode(lang, OdiseaCode);
        }

        #endregion

        #region Fly And Drive
        [HttpGet]
        [ActionName("GetFlyAndDriveCars")]
        public List<FlyAndDriveCar> GetFlyAndDriveCars(string id)
        {
            string lang = getLangFromHeader();

            return CarsRepository.GetFlyAndDriveCars(lang, id);
        }
        #endregion

        #region Subjects
        [HttpGet]
        [ActionName("GetAllSubjects")]
        public List<WTC.Models.Subject> GetAllSubjects()
        {
            string lang = getLangFromHeader();

            return SubjectRepository.GetAllSubjects(lang);
        }

        #endregion
        #endregion

        #region Menu & Footer
        [HttpGet]
        [ActionName("GetMainMenu")]
        public MainMenu GetMainMenu()
        {
            string lang = getLangFromHeader();

            return LayoutRepository.GetMainMenu(lang);

        }

        [HttpGet]
        [ActionName("GetMainMenuTree")]
        public MainMenu GetMainMenuTree()
        {
            string lang = getLangFromHeader();

            return LayoutRepository.GetMainMenuTree(lang);

        }
        //1175

        [HttpGet]
        [ActionName("GetFooter")]
        public Footer GetFooter()
        {
            string lang = getLangFromHeader();

            return LayoutRepository.GetFooter(lang).FirstOrDefault();
        }
        #endregion

        #region Lobby Pages
        [HttpGet]
        [ActionName("GetLobbyPageByID")]
        public LobbyPage GetLobbyPageByID(string id)
        {
            string lang = getLangFromHeader();

            return LobbyPageRepository.GetLobbyPageByID(lang, id).FirstOrDefault();
        }
        
        [HttpGet]
        [ActionName("GetLobbyPage")]
        public LobbyPage GetLobbyPageTemplate(int LobbyPageTemplatesEnumID, string ObjectID = "", string SecondObjectID = "")
        {
            string lang = getLangFromHeader();

            return LobbyPageRepository.GetLobbyPageTemplate(lang, LobbyPageTemplatesEnumID, ObjectID).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("GetAllDestinationsPages")]
        public List<LobbyPage> GetAllDestinationsPages()
        {
            string lang = getLangFromHeader();

            return LobbyPageRepository.GetAllDestinationsPages(lang);
        }

        [HttpGet]
        [ActionName("GetAllSegmentsPages")]
        public List<LobbyPage> GetAllSegmentsPages()
        {
            string lang = getLangFromHeader();

            return LobbyPageRepository.GetAllSegmentsPages(lang);
        }

        [HttpGet]
        [ActionName("GetAllSubjectsPages")]
        public List<LobbyPage> GetAllSubjectsPages()
        {
            string lang = getLangFromHeader();

            return LobbyPageRepository.GetAllSubjectsPages(lang);
        }

        [HttpGet]
        [ActionName("GetAllSegmentInDestinationPages")]
        public List<LobbyPage> GetAllSegmentInDestinationPages()
        {
            string lang = getLangFromHeader();

            return LobbyPageRepository.GetAllSegmentInDestinationPages(lang);
        }

        [HttpGet]
        [ActionName("GetAllGeneralPages")]
        public List<LobbyPage> GetAllGeneralPages()
        {
            string lang = getLangFromHeader();

            return LobbyPageRepository.GetAllGeneralPages(lang);
        }
        #endregion

        #region Promotions
        //[HttpGet]
        //[ActionName("GetLobbyPage")]
        //public LobbyPage GetLobbyPage(int LobbyPageTemplatesEnumID, string ObjectID = "")
        //{
        //    return PromotionsRepository.GetLobbyPage(LobbyPageTemplatesEnumID, ObjectID).FirstOrDefault();
        //}

        //[HttpPost]

        [HttpGet]
        [ActionName("GetDestinationPromotions")]
        public List<Promotion> GetDestinationPromotions(string id)
        {
            string lang = getLangFromHeader();

            return PromotionsRepository.GetDestinationPromotions(lang, id);
        }

        [HttpGet]
        [ActionName("GetQueryPromotions")]
        public List<Promotion> GetQueryPromotions(string destinationID, string segmentID, string subjectID)
        {
            string lang = getLangFromHeader();

            return PromotionsRepository.GetQueryPromotions(lang, destinationID, segmentID, subjectID);
        }

        [HttpGet]
        [ActionName("GetAllPromotions")]
        public List<Promotion> GetAllPromotions()
        {

            string lang = getLangFromHeader();

            return PromotionsRepository.GetAllPromotions(lang);
        }
        #endregion

        #region Static Pages
        [HttpGet]
        [ActionName("GetStaticPage")]
        public List<StaticPage> GetStaticPage(string id)
        {

            string lang = getLangFromHeader();

            return StaticPagesRepository.GetStaticPage(lang, id);
        }

        [HttpGet]
        [ActionName("GetAboutUsPage")]
        public AboutUs GetAboutUsPage()
        {
            string lang = getLangFromHeader();

            return StaticPagesRepository.GetAboutUsPage(lang).FirstOrDefault();
        }

        [HttpGet]
        [ActionName("GetTermsPage")]
        public AboutUs GetTermsPage()
        {
            string lang = getLangFromHeader();

            return StaticPagesRepository.GetTermsPage(lang).FirstOrDefault();
        }


        [HttpGet]
        [ActionName("GetContactUsPage")]
        public ContactUs GetContactUsPage()
        {
            string lang = getLangFromHeader();

            return StaticPagesRepository.GetContactUsPage(lang).FirstOrDefault();
        }

        #endregion

        #region Settings
        [HttpGet]
        [ActionName("GetAirlines")]
        public List<Airline> GetAirlines(string id)
        {
            string lang = getLangFromHeader();

            return SettingsRepository.GetAirlines(lang, id);
        }

        [HttpGet]
        [ActionName("GetBoardBases")]
        public List<BoardBase> GetBoardBases(string id)
        {
            string lang = getLangFromHeader();

            return SettingsRepository.GetBoardBases(lang, id);
        }

        [HttpGet]
        [ActionName("GetCurrencies")]
        public List<Currency> GetCurrencies(string id)
        {
            string lang = getLangFromHeader();

            return SettingsRepository.GetCurrencies(lang, id);
        }

        [HttpGet]
        [ActionName("GetHotelTypes")]
        public List<HotelType> GetHotelTypes(string id)
        {
            string lang = getLangFromHeader();

            return SettingsRepository.GetHotelTypes(lang, id);
        }

        [HttpGet]
        [ActionName("GetLanguages")]
        public List<Language> GetLanguages(string id)
        {
            string lang = getLangFromHeader();

            return SettingsRepository.GetLanguages(lang, id);
        }

        [HttpGet]
        [ActionName("GetSegments")]
        public List<Segment> GetSegments(string id)
        {
            string lang = getLangFromHeader();

            return SettingsRepository.GetSegments(lang, id);
        }

        [HttpGet]
        [ActionName("GetStamps")]
        public List<Stamp> GetStamps(string id)
        {
            string lang = getLangFromHeader();

            return SettingsRepository.GetStamps(lang, id);
        }

        [HttpGet]
        [ActionName("GetSiteInfo")]
        public SiteInfo GetSiteInfo()
        {
            string lang = getLangFromHeader();

            return StaticPagesRepository.GetSiteInfo(lang).FirstOrDefault();
        }
        #endregion

        #region Mail Templates
        [HttpGet]
        [ActionName("GetConfirmationMailTemplate")]
        public ConfirmationEmailTemplate GetConfirmationMailTemplate()
        {
            string lang = getLangFromHeader();

            return MailTemplatesRepository.GetConfirmationEmailTemplate(lang).FirstOrDefault();
        }
        #endregion


        public string getLangFromHeader()
        {
            string lang = "";
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("lang"))
            {
                lang = headers.GetValues("lang").First();
            }
            else
            {
                lang = WTC.Utils.VortoUtils.GetDefaultLang();
            }
            //else if (headers.AcceptLanguage != null)
            //{
            //    if (headers.AcceptLanguage.First() != null)
            //    {
            //        lang = headers.AcceptLanguage.First().Value;                    
            //    }
            //}

            return lang;
        }
    }
}