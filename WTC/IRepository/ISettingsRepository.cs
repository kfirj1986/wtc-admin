﻿using System;
using System.Collections.Generic;
namespace WTC.IRepository
{
    public interface ISettingsRepository
    {
        List<WTC.Models.Airline> GetAirlines(string lang , string id);
        List<WTC.Models.BoardBase> GetBoardBases(string lang , string id);
        List<WTC.Models.Currency> GetCurrencies(string lang , string id);
        List<WTC.Models.HotelType> GetHotelTypes(string lang , string id);
        List<WTC.Models.Language> GetLanguages(string lang , string id);
        List<WTC.Models.Segment> GetSegments(string lang , string id);
        List<WTC.Models.Stamp> GetStamps(string lang , string id);
        
    }
}
