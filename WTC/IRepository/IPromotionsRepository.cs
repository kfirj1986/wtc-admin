﻿using System;
namespace WTC.IRepository
{
    public interface IPromotionsRepository
    {
        System.Collections.Generic.List<WTC.Models.Promotion> GetDestinationPromotions(string lang,string id);
        System.Collections.Generic.List<WTC.Models.Promotion> GetQueryPromotions(string lang,string destinationID, string segmentID, string subjectID);
        System.Collections.Generic.List<Models.Promotion> GetAllPromotions(string lang);

        

        
    }
}
