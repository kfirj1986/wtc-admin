﻿using System;
namespace WTC.IRepository
{
    public interface ISubjectRepository
    {
        System.Collections.Generic.List<WTC.Models.Subject> GetAllSubjects(string lang);

    }
}
