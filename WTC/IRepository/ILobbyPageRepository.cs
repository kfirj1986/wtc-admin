﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTC.IRepository
{
    public interface ILobbyPageRepository
    {
        System.Collections.Generic.List<WTC.Models.LobbyPage> GetLobbyPageTemplate(string lang,int LobbyPageTemplatesEnumID, string ObjectID, string SecondObjectID = "");

        List<Models.LobbyPage> GetAllDestinationsPages(string lang);
        List<Models.LobbyPage> GetAllSegmentsPages(string lang);
        List<Models.LobbyPage> GetAllSubjectsPages(string lang);
        List<Models.LobbyPage> GetAllSegmentInDestinationPages(string lang);
        List<Models.LobbyPage> GetAllGeneralPages(string lang);

        List<Models.LobbyPage> GetLobbyPageByID(string lang, string id);
    }
}