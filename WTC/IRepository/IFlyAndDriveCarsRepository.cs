﻿using System;
namespace WTC.IRepository
{
    public interface IFlyAndDriveCarsRepository
    {
        System.Collections.Generic.List<WTC.Models.FlyAndDriveCar> GetFlyAndDriveCars(string lang, string id);
    }
}