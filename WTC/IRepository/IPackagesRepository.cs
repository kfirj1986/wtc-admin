﻿using System;
namespace WTC.IRepository
{
    public interface IPackagesRepository
    {
        System.Collections.Generic.List<WTC.Models.Package> GetPackages(string lang,string id);
        System.Collections.Generic.List<WTC.Models.Package> GetPackageByOdiseaCode(string lang,string OdiseaCode);

        
    }
}
