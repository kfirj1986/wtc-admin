﻿using System;
namespace WTC.IRepository
{
    public interface IOrganizedTripsRepository
    {
        System.Collections.Generic.List<WTC.Models.Guide> GetGuides(string lang,string id);
        System.Collections.Generic.List<WTC.Models.OrganizedTrip> GetOrganizedTrips(string lang, string id);
        System.Collections.Generic.List<WTC.Models.OrganizedTrip> GetAllTripsByDestinationID(string lang, string id);
        System.Collections.Generic.List<WTC.Models.OrganizedTrip> GetOrganizedTripsTree(string lang);

        WTC.Models.OrganizedTrip GetOrganizedTripsByID(string lang, int id);
        
        
    }
}
