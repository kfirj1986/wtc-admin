﻿using System;
namespace WTC.IRepository
{
    public interface IStaticPagesRepository
    {
        System.Collections.Generic.List<WTC.Models.StaticPage> GetStaticPage(string lang,string id);
        System.Collections.Generic.List<WTC.Models.HomePage> GetHomePage(string lang);
        System.Collections.Generic.List<WTC.Models.AboutUs> GetAboutUsPage(string lang);
        System.Collections.Generic.List<WTC.Models.ContactUs> GetContactUsPage(string lang);

        System.Collections.Generic.List<WTC.Models.AboutUs> GetTermsPage(string lang);

        System.Collections.Generic.List<Models.SiteInfo> GetSiteInfo(string lang);

        
    }
}
