﻿using System;
namespace WTC.IRepository
{
    public interface IDestinationsRepository
    {
        System.Collections.Generic.List<WTC.Models.Area> GetAreas(string lang , string id);
        System.Collections.Generic.List<WTC.Models.City> GetCities(string lang , string id);
        System.Collections.Generic.List<WTC.Models.Airport> GetAirPorts(string lang , string id);
        
        System.Collections.Generic.List<WTC.Models.Country> GetCountries(string lang , string id);
        System.Collections.Generic.List<Models.Continent> GetContinents(string lang , string id);
        System.Collections.Generic.List<WTC.Models.Destination> GetAllDestinations(string lang);
        System.Collections.Generic.List<WTC.Models.Destination> GetDestinationTree(string lang);

        System.Collections.Generic.List<WTC.Models.Destination> GetDestinationByID(string lang , int id);
        WTC.Models.Destination GetDestinationTreeByID(string lang , int id);

    }
}
