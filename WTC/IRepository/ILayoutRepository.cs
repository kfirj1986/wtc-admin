﻿using System;
namespace WTC.IRepository
{
    public interface ILayoutRepository
    {
        System.Collections.Generic.List<WTC.Models.Footer> GetFooter(string lang);
        WTC.Models.MainMenu GetMainMenu(string lang);

        WTC.Models.MainMenu GetMainMenuTree(string lang);
    }
}
