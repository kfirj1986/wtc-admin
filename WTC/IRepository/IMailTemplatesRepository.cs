﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WTC.IRepository
{
    public interface IMailTemplatesRepository
    {
        System.Collections.Generic.List<WTC.Models.ConfirmationEmailTemplate> GetConfirmationEmailTemplate(string lang);

    }
}