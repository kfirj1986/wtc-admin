﻿using System;
namespace WTC.IRepository
{
    public interface IHotelsRepository
    {
        System.Collections.Generic.List<WTC.Models.Hotel> GetHotels(string lang,string id, string areaId = "", string countryId = "", string cityId = "");
        System.Collections.Generic.List<WTC.Models.Hotel> GetHotelByCityIDAndName(string lang,string cityId, string name);
        WTC.Models.Hotel GetHotelTreeByID(string lang,string id);

        
    }
}
