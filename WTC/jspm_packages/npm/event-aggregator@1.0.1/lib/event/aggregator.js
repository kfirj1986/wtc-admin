/* */ 
var util = require('util');
var EventEmitter = require('events').EventEmitter;
function EventAggregator() {
  EventEmitter.call(this);
  this._eventList = {};
}
util.inherits(EventAggregator, EventEmitter);
EventAggregator.prototype._getEventList = function($trigger) {
  if (!this._eventList[$trigger]) {
    this._eventList[$trigger] = [];
  }
  return this._eventList[$trigger];
};
EventAggregator.prototype.waitCallback = function($trigger) {
  var triggers = (util.isArray($trigger)) ? $trigger : [$trigger];
  var _self = this;
  var pack = {
    type: 'callback',
    trigger: triggers,
    isFired: 0
  };
  var callback = function() {
    _self._respond(pack);
  };
  callback.data = pack;
  for (var i = 0; i < triggers.length; i++) {
    this._getEventList(triggers[i]).push(pack);
  }
  return callback;
};
EventAggregator.prototype.ignoreCallback = function($trigger, $callback) {
  var triggers = (util.isArray($trigger)) ? $trigger : [$trigger];
  for (var i = 0; i < triggers.length; i++) {
    var list = this._getEventList(triggers[i]);
    var i = list.indexOf($callback.data);
    if (i != -1) {
      list.splice(i, 1);
    }
  }
};
EventAggregator.prototype.waitEvent = function($trigger, $emitter, $event) {
  var triggers = (util.isArray($trigger)) ? $trigger : [$trigger];
  var _self = this;
  var pack = {
    type: 'event',
    trigger: triggers,
    emitter: $emitter,
    event: $event,
    isFired: 0
  };
  pack.listener = function() {
    _self._respond(pack);
  };
  $emitter.addListener($event, pack.listener);
  for (var i = 0; i < triggers.length; i++) {
    this._getEventList(triggers[i]).push(pack);
  }
};
EventAggregator.prototype.ignoreEvent = function($trigger, $emitter, $event) {
  var triggers = (util.isArray($trigger)) ? $trigger : [$trigger];
  for (var i = 0; i < triggers.length; i++) {
    var list = this._getEventList(triggers[i]).filter(function($element, $index, $array) {
      if ($element.emitter == $emitter && $element.event == $event) {
        $element.emitter.removeListener($element.event, $element.listener);
        return false;
      } else {
        return true;
      }
    }, this);
    this._eventList[triggers[i]] = list;
  }
};
EventAggregator.prototype.resetTrigger = function($trigger) {
  var triggers = (util.isArray($trigger)) ? $trigger : [$trigger];
  for (var i = 0; i < triggers.length; i++) {
    var list = this._getEventList(triggers[i]);
    for (var i = 0; i < list.length; i++) {
      list[i].isFired = 0;
    }
  }
};
EventAggregator.prototype.clearTrigger = function($trigger) {
  var triggers = (util.isArray($trigger)) ? $trigger : [$trigger];
  for (var i = 0; i < triggers.length; i++) {
    this._eventList[triggers[i]] = [];
  }
};
EventAggregator.prototype._respond = function($package) {
  var triggers = $package.trigger;
  var trigger = triggers[0];
  if (this._getEventList(trigger).indexOf($package) != -1) {
    if (!$package.isFired) {
      $package.isFired++;
      this._checkEmit(trigger);
    }
  }
};
EventAggregator.prototype._checkEmit = function($trigger) {
  if (this.check($trigger)) {
    this.emit($trigger);
  }
};
EventAggregator.prototype.check = function($trigger) {
  var list = this._getEventList($trigger);
  if (!list.length)
    return false;
  for (var i = 0; i < list.length; i++) {
    if (!list[i].isFired) {
      return false;
    }
  }
  return true;
};
module.exports = {EventAggregator: EventAggregator};
