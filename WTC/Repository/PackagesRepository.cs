﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.IRepository;

namespace WTC.Repository
{
    public class PackagesRepository : BaseUmbracoContextRepository, IPackagesRepository
    {
        public List<WTC.Models.Package> GetPackages(string lang, string id)
        {
            return ReturnResults<WTC.Models.Package>(lang,"package", id);
        }

        public List<WTC.Models.Package> GetPackageByOdiseaCode(string lang, string OdiseaCode)
        {
            Dictionary<string, string> conditions = new Dictionary<string, string>();
            conditions.Add("packageCode", OdiseaCode);
            return ReturnResults<WTC.Models.Package>(lang,"package", conditions);
        }
    }
}