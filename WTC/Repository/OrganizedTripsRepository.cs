﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.IRepository;
using WTC.Models;

namespace WTC.Repository
{
    public class OrganizedTripsRepository : BaseUmbracoContextRepository, IOrganizedTripsRepository
    {
        public List<OrganizedTrip> GetOrganizedTrips(string lang, string id)
        {
            return ReturnResults<OrganizedTrip>(lang,"organizedTrip", id);
        }

        public List<OrganizedTrip> GetAllTripsByDestinationID(string lang, string id)
        {
            Dictionary<string, string> conditions = new Dictionary<string, string>();
            conditions.Add("tripDestinations", id);
            return ReturnResults<OrganizedTrip>(lang,"organizedTrip", conditions);
        }

        public List<OrganizedTrip> GetOrganizedTripsTree(string lang)
        {
            List<OrganizedTrip> organizedTripsTree = new List<OrganizedTrip>();
            List<OrganizedTrip> organizedTripList = ReturnResults<OrganizedTrip>(lang,"organizedTrip");
            foreach (OrganizedTrip organizedTrip in organizedTripList)
            {
                organizedTripsTree.Add(GetOrganizedTripsByID(lang,Convert.ToInt32(organizedTrip.id)));
            }
            return organizedTripsTree;
        }


        public OrganizedTrip GetOrganizedTripsByID(string lang, int id)
        {
            //return ReturnResults<OrganizedTrip>("organizedTrip", id);
            umbraco.NodeFactory.Node oOrganizedTrip = umbraco.uQuery.GetNode(id);
            return ReturnResults<OrganizedTrip>(lang,oOrganizedTrip);
        }


        public List<Guide> GetGuides(string lang, string id)
        {
            return ReturnResults<Guide>(lang,"guide", id);
        }
    }
}