﻿using Examine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UmbracoExamine;
using WTC.IRepository;
using WTC.Models;
using WTC.Utils;

namespace WTC.Repository
{
    public class HotelsRepository : BaseUmbracoContextRepository, IHotelsRepository
    {
        public List<Hotel> GetHotels(string lang,string id, string areaId = "", string countryId = "", string cityId = "")
        {
            Dictionary<string, string> conditions = new Dictionary<string, string>();
            conditions.Add("areaId", areaId.ToString());
            conditions.Add("countryId", countryId.ToString());
            conditions.Add("cityId", cityId.ToString());
            conditions.Add("id", id);

            return ReturnResults<Hotel>(lang,"hotel", conditions);

            #region Old Code
            //foreach (var searchResult in searchResults)
            //{
            //    umbraco.NodeFactory.Node oHotel = umbraco.uQuery.GetNode(searchResult.Id);
            //    if (oHotel != null)
            //    {
            //        Models.Hotel hotelresult = new Models.Hotel();

            //        hotelresult.Name = getPropValue(oHotel, "hotelName");

            //        hotelresult.HotelGWID = getPropValue(oHotel, "gWID");


            //        if (oHotel.GetProperty("city") != null)
            //        {
            //            umbraco.NodeFactory.Node oCity = umbraco.uQuery.GetNode(oHotel.GetProperty("city").Value);
            //            if (oCity != null)
            //            {
            //                hotelresult.CityName = getPropValue(oCity, "cityName");
            //                hotelresult.CityId = oCity.Id.ToString();
            //            }
            //        }

            //        if (oHotel.Children != null)
            //        {
            //            foreach (var hotelRoom in oHotel.ChildrenAsList)
            //            {
            //                umbraco.NodeFactory.Node oRoom = umbraco.uQuery.GetNode(hotelRoom.Id);
            //                if (oRoom != null)
            //                {
            //                    WTC.Models.HotelRoom oHotelRoom = new Models.HotelRoom();

            //                    oHotelRoom.Name = getPropValue(oRoom, "roomName");
            //                    oHotelRoom.Description = getPropValue(oRoom, "roomDescription");
            //                    oHotelRoom.Size = getPropValue(oRoom, "size");

            //                    hotelresult.Rooms.Add(oHotelRoom);
            //                }
            //            }
            //        }

            //        hotelResults.Add(hotelresult);
            //    }
            //} 
            #endregion
        }

        public Hotel GetHotelTreeByID(string lang, string id)
        {
            umbraco.NodeFactory.Node oHotel = umbraco.uQuery.GetNode(id);
            //umbraco.cms.businesslogic.web.Document doc = new umbraco.cms.businesslogic.web.Document(int.Parse(id));
            return ReturnResults<Hotel>(lang,oHotel);
        }

        public List<Hotel> GetHotelByCityIDAndName(string lang, string cityId, string name)
        {
            Dictionary<string, string> conditions = new Dictionary<string, string>();
            conditions.Add("city", cityId);
            conditions.Add("mainName", name);

            return ReturnResults<Hotel>(lang,"hotel", conditions);
        }
    }
}