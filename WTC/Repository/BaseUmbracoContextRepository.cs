﻿using Examine;
using Examine.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using WTC.Utils;

namespace WTC.Repository
{
    public class BaseUmbracoContextRepository
    {
        public readonly BaseSearchProvider ExamineSearcherProvider;
        public readonly UmbracoHelper CmsContext;

        public BaseUmbracoContextRepository()
        {
            ExamineSearcherProvider = ExamineManager.Instance.SearchProviderCollection[WTC.Utils.ExamineUtilsExtensionMethods.GetSearcher(false)];
            CmsContext = new UmbracoHelper(UmbracoContext.Current);
        }

        protected List<T> ReturnResults<T>(string lang, string nodeTypeAlias, string id)
        where T : class,new()
        {
            Examine.SearchCriteria.IBooleanOperation query = ExamineUtils.GetQueryByNodeTypeAlias(nodeTypeAlias, id);

            return SearchAndMap<T>(lang, query);
        }


        protected List<T> ReturnResults<T>(string lang, string nodeTypeAlias, Dictionary<string, string> conditions)
where T : class,new()
        {
            Examine.SearchCriteria.IBooleanOperation query = ExamineUtils.GetQueryByNodeTypeAlias(nodeTypeAlias, conditions);
            return SearchAndMap<T>(lang, query);
        }

        protected List<T> ReturnResults<T>(string lang, params string[] nodeTypeAliases)
where T : class,new()
        {
            Examine.SearchCriteria.IBooleanOperation query = ExamineUtils.GetGroupQueryByNodeTypeAliases(nodeTypeAliases);

            return SearchAndMap<T>(lang, query);
        }

        protected List<T> ReturnResults<T>(string lang, int id, params string[] nodeTypeAliases)
where T : class,new()
        {
            Examine.SearchCriteria.IBooleanOperation query = ExamineUtils.GetGroupQueryByNodeTypeAliasesAndId(id, nodeTypeAliases);

            return SearchAndMap<T>(lang, query);
        }

        private List<T> SearchAndMap<T>(string lang, Examine.SearchCriteria.IBooleanOperation query) where T : class, new()
        {
            var searchResults = ExamineSearcherProvider.Search(query.Compile());
            if (searchResults != null)
            {
                return Utils.ExamineUtils.MapSearchResultsToModel<T>(searchResults, lang).ToList();
            }
            return null;
        }


        #region Added By Jarufi 4/5/2016
        protected T ReturnResults<T>(string lang, umbraco.NodeFactory.Node node)
where T : class,new()
        {
            return MapSearchResultNodeItemToModel<T>(lang,node);
        }


        public static T MapSearchResultNodeItemToModel<T>(string lang, umbraco.NodeFactory.Node node) where T : new()
        {
            return MapperUtils.Map<T>(node, lang);
        }

        #endregion
    }
}