﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Models;
using UmbracoExamine;
using WTC.Utils;
using Examine;
using WTC.IRepository;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace WTC.Repository
{
    public class StaticPagesRepository : BaseUmbracoContextRepository, IStaticPagesRepository
    {
        public List<WTC.Models.StaticPage> GetStaticPage(string lang,string id)
        {
            return ReturnResults<WTC.Models.StaticPage>(lang, "staticPage", id);
        }
        

        public List<HomePage> GetHomePage(string lang)
        {

            List<HomePage> HomePage = ReturnResults<HomePage>(lang,"homePage");

            //if (LobbyPage != null)
            //{
            //    foreach (LobbyPage lobbyPage in LobbyPage)
            //    {
            //        if (lobbyPage.contentComponents != null)
            //        {
            //            foreach (var gallery in lobbyPage.contentComponents)
            //            {
            //                if (!string.IsNullOrEmpty(gallery.Promotions))
            //                {
            //                    gallery.PromotionList = new List<Promotion>();

            //                    foreach (var item in gallery.Promotions.Split(',').ToList())
            //                    {
            //                        umbraco.NodeFactory.Node oPromotion = umbraco.uQuery.GetNode(item);
            //                        gallery.PromotionList.Add(ReturnResults<Promotion>(oPromotion));
            //                    }
            //                }
            //            } 
            //        }
            //    }
            //}

            return HomePage;
        }


        public List<AboutUs> GetAboutUsPage(string lang)
        {
            return ReturnResults<AboutUs>(lang,"aboutUs", "");
        }

        public List<ContactUs> GetContactUsPage(string lang)
        {
            return ReturnResults<ContactUs>(lang,"contactUs");
        }

        public List<AboutUs> GetTermsPage(string lang)
        {
            return ReturnResults<AboutUs>(lang,"termsAndConditions");
        }

        public List<SiteInfo> GetSiteInfo(string lang)
        {
            return ReturnResults<SiteInfo>(lang,"siteInfo");
        }

    }
}