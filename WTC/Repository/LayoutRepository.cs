﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.IRepository;
using WTC.Models;

namespace WTC.Repository
{
    public class LayoutRepository : BaseUmbracoContextRepository, ILayoutRepository
    {
        public MainMenu GetMainMenu(string lang)
        {
            MainMenu oMainMenu = new MainMenu();
            oMainMenu.MenuItem = ReturnResults<MenuItem>(lang,"menuItem", "");
            return oMainMenu;
        }

        public MainMenu GetMainMenuTree(string lang)
        {
            umbraco.NodeFactory.Node oMenu = umbraco.uQuery.GetNode(1175);
            return ReturnResults<MainMenu>(lang,oMenu);
        }   

        public List<Footer> GetFooter(string lang)
        {
            return ReturnResults<Footer>(lang,"footer", "");
        }
        
    }
}