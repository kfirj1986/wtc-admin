﻿using Examine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UmbracoExamine;
using WTC.IRepository;
using WTC.Models;
using WTC.Utils;

namespace WTC.Repository
{
    public class FlyAndDriveCarsRepository : BaseUmbracoContextRepository, IFlyAndDriveCarsRepository
    {
        public List<FlyAndDriveCar> GetFlyAndDriveCars(string lang, string id)
        {
            return ReturnResults<WTC.Models.FlyAndDriveCar>(lang, "flyAndDriveCar", id);
        }
    }
}