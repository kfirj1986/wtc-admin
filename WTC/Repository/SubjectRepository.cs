﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Models;
using UmbracoExamine;
using WTC.Utils;
using Examine;
using WTC.IRepository;
using Umbraco.Core.Models;
using Umbraco.Web;
using WTC.Repository;

namespace WTC.Repository
{
    public class SubjectRepository : BaseUmbracoContextRepository, ISubjectRepository
    {
        public List<WTC.Models.Subject> GetAllSubjects(string lang)
        {
            return ReturnResults<WTC.Models.Subject>(lang,"subject");
        }

    }
}