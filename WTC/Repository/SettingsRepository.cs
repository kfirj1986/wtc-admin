﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Models;
using UmbracoExamine;
using WTC.Utils;
using Examine;
using WTC.IRepository;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace WTC.Repository
{
    public class SettingsRepository : BaseUmbracoContextRepository, ISettingsRepository
    {
        public List<WTC.Models.Airline> GetAirlines(string lang , string id)
        {
            return ReturnResults<WTC.Models.Airline>(lang,"airline", id);
        }

        public List<WTC.Models.BoardBase> GetBoardBases(string lang , string id)
        {
            return ReturnResults<WTC.Models.BoardBase>(lang, "boardBase", id);
        }

        public List<WTC.Models.Currency> GetCurrencies(string lang , string id)
        {
            return ReturnResults<WTC.Models.Currency>(lang, "currency", id);
        }

        public List<WTC.Models.HotelType> GetHotelTypes(string lang , string id)
        {
            return ReturnResults<WTC.Models.HotelType>(lang, "hotelType", id);
        }

        public List<WTC.Models.Language> GetLanguages(string lang , string id)
        {
            return ReturnResults<WTC.Models.Language>(lang, "language", id);
        }

        public List<WTC.Models.Segment> GetSegments(string lang , string id)
        {
            return ReturnResults<WTC.Models.Segment>(lang, "segment", id);
        }

        public List<WTC.Models.Stamp> GetStamps(string lang , string id)
        {
            return ReturnResults<WTC.Models.Stamp>(lang, "stamp", id);
        }
    }
}