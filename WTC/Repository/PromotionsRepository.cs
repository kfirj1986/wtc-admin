﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.IRepository;
using WTC.Models;

namespace WTC.Repository
{
    public class PromotionsRepository : BaseUmbracoContextRepository, IPromotionsRepository
    {

        public List<Promotion> GetDestinationPromotions(string lang,string id)
        {
            Dictionary<string, string> conditions = new Dictionary<string, string>();
            conditions.Add("destinationPicker", id);
            return ReturnResults<Promotion>(lang,"promotion", conditions);

        }


        public List<Promotion> GetQueryPromotions(string lang,string destinationID, string segmentID, string subjectID)
        {
            Dictionary<string, string> conditions = new Dictionary<string, string>();
            conditions.Add("destinations", destinationID);
            conditions.Add("segment", segmentID);
            conditions.Add("subjects", subjectID);            
            return ReturnResults<Promotion>(lang,"promotion", conditions);

        }

        public List<Promotion> GetAllPromotions(string lang)
        {
            return ReturnResults<Promotion>(lang,"promotion");
        }
        


        #region Utils Methods
        #endregion

    }
}