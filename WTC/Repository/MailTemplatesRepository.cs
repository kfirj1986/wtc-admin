﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.IRepository;

namespace WTC.Repository
{
    public class MailTemplatesRepository : BaseUmbracoContextRepository, IMailTemplatesRepository
    {
        public List<WTC.Models.ConfirmationEmailTemplate> GetConfirmationEmailTemplate(string lang)
        {
            return ReturnResults<WTC.Models.ConfirmationEmailTemplate>(lang, "confirmationEmailTemplate");
        }
    }
}