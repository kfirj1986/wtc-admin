﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.IRepository;
using WTC.Models;

namespace WTC.Repository
{
    public class LobbyPageRepository : BaseUmbracoContextRepository, ILobbyPageRepository
    {
        public List<LobbyPage> GetLobbyPageTemplate(string lang, int LobbyPageTemplatesEnumID, string ObjectID, string SecondObjectID = "")
        {
            ELobbyPageTemplates LobbyPageAlias = (ELobbyPageTemplates)LobbyPageTemplatesEnumID;
            Dictionary<string, string> queryConditions = new Dictionary<string, string>();

            switch (LobbyPageAlias)
            {
                case ELobbyPageTemplates.destinationPage:
                    queryConditions.Add("destination", ObjectID);
                    break;
                case ELobbyPageTemplates.segmentInDestinationPage:
                    queryConditions.Add("segment", ObjectID);
                    queryConditions.Add("destination", SecondObjectID);
                    break;
                case ELobbyPageTemplates.segmentPage:
                    queryConditions.Add("segment", ObjectID);
                    break;
                case ELobbyPageTemplates.subjectPage:
                    queryConditions.Add("subject", ObjectID);
                    break;
                default:
                    break;
            }

            List<LobbyPage> LobbyPage = ReturnResults<LobbyPage>(lang, LobbyPageAlias.ToString(), queryConditions);

            return LobbyPage;
        }


        public List<LobbyPage> GetAllDestinationsPages(string lang)
        {
            return ReturnResults<LobbyPage>(lang, "destinationPage");
        }

        public List<LobbyPage> GetAllSegmentsPages(string lang)
        {
            return ReturnResults<LobbyPage>(lang, "segmentPage");
        }

        public List<LobbyPage> GetAllSubjectsPages(string lang)
        {
            return ReturnResults<LobbyPage>(lang, "subjectPage");
        }

        public List<LobbyPage> GetAllSegmentInDestinationPages(string lang)
        {
            return ReturnResults<LobbyPage>(lang, "segmentInDestinationPage");
        }

        public List<LobbyPage> GetAllGeneralPages(string lang)
        {
            return ReturnResults<LobbyPage>(lang, "generalPage");
        }

        public List<LobbyPage> GetLobbyPageByID(string lang, string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            return ReturnResults<LobbyPage>(lang, Convert.ToInt32(id), "destinationPage", "segmentPage", "subjectPage", "segmentInDestinationPage", "generalPage");
        }



    }
}