﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WTC.Models;
using UmbracoExamine;
using WTC.Utils;
using Examine;
using WTC.IRepository;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace WTC.Repository
{
    public class DestinationsRepository : BaseUmbracoContextRepository, IDestinationsRepository
    {
        public List<Destination> GetAllDestinations(string lang)
        {
            //return ReturnResults<Destination>(lang,"country", "area", "city", "continent");
            List<Destination> destinations = ReturnResults<Destination>(lang, "country", "area", "city", "continent");
            EnrichFriedlyUrl(lang, destinations);
            return destinations;
        }

        public List<Destination> GetDestinationByID(string lang, int id)
        {
            return ReturnResults<Destination>(lang, id, "country", "area", "city", "continent");
        }


        public Destination GetDestinationTreeByID(string lang, int id)
        {
            umbraco.NodeFactory.Node oDestination = umbraco.uQuery.GetNode(id);
            return ReturnResults<Destination>(lang, oDestination);
        }

        public List<Destination> GetDestinationTree(string lang)
        {
            List<Destination> destinationsTree = new List<Destination>();
            List<Continent> continentList = ReturnResults<Continent>(lang, "continent");
            foreach (Continent continent in continentList)
            {
                destinationsTree.Add(GetDestinationTreeByID(lang, Convert.ToInt32(continent.id)));
            }

            EnrichFriedlyUrl(lang, destinationsTree);

            return destinationsTree;
        }

        public List<Country> GetCountries(string lang, string id)
        {
            return ReturnResults<Country>(lang, "country", id);
        }


        public List<Continent> GetContinents(string lang, string id)
        {
            return ReturnResults<Continent>(lang, "continent", id);
        }


        public List<City> GetCities(string lang, string id)
        {
            return ReturnResults<City>(lang, "city", id);
        }

        public List<Airport> GetAirPorts(string lang, string id)
        {
            return ReturnResults<Airport>(lang, "airport", id);
        }



        public List<Area> GetAreas(string lang, string id)
        {
            return ReturnResults<Area>(lang, "area", id);
        }


        #region Helpers
        private void EnrichFriedlyUrl(string lang, List<Destination> destinationsTree, List<LobbyPage> destinationPages = null)
        {
            if (destinationPages == null) { destinationPages = ReturnResults<LobbyPage>(lang, "destinationPage"); }

            foreach (Destination destination in destinationsTree)
            {
                if (destination.city != null)
                {
                    EnrichFriedlyUrl(lang, destination.city, destinationPages);
                }
                else if (destination.country != null)
                {
                    EnrichFriedlyUrl(lang, destination.country, destinationPages);
                }
                else
                {
                    //NO INNER LIST
                }

                var tempDestLobbyPage = destinationPages.FirstOrDefault(dlp => dlp.destination == destination.id && dlp.Seo != null);
                if (tempDestLobbyPage != null)
                {
                    destination.lobbyPageFriendlyURL = tempDestLobbyPage.Seo.friendlyURL;
                }
            }
        }
        #endregion
    }
}